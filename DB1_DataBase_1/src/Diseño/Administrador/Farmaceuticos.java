/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Administrador;

import BaseDeDatosConexion.ConexionBD;
import Datos.Metodos;
import Modelo.Usuarios.PFarmaceutico.Farmaceutico;
import funcional.Util;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Domenica Barreiro
 */
public class Farmaceuticos {

    private TableView<Farmaceutico> tvFarmaceuticos;
    private double ancho;
    private double alto;
    private Tab Farmaceuticos;
    private Stage origen;

    public Farmaceuticos(Stage stage) throws FileNotFoundException {

        this.origen = stage;
        ancho = stage.getWidth();
        alto = stage.getHeight();

        Farmaceuticos = new Tab("Farmacéuticos                 ");
        VBox vb = new VBox(arriba(), grilla());
        cargarGrilla();
        vb.setSpacing(20);
        vb.setStyle("-fx-background-image: url('" + getClass().getResource("/recursos/fondo_farma.jpg").toExternalForm() + "');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: " + ancho + " " + alto + ";"
                + "-fx-background-position: center;");

        Farmaceuticos.setContent(vb);
        Farmaceuticos.setClosable(false);
    }

    public Tab getFarmaceuticos() {
        return Farmaceuticos;
    }

    private VBox arriba() throws FileNotFoundException {
        
        ImageView im = Util.imagen("src/recursos/logo.png", 200, 100);
        VBox logo = new VBox(im);
        
        Button btnNuevo = new Button("Nuevo Farmacéutico");
        btnNuevo.setOnAction(e -> {
            NuevoFarmaceutico pn = new NuevoFarmaceutico(origen,this);
            Stage main = pn.getNuevoFarmaceutico();
            main.show();
        });

        VBox vb = new VBox(logo,btnNuevo);
        vb.setSpacing(10);
        vb.setPadding(new Insets(0,0,20,0));
        vb.setTranslateX(20);
        vb.setTranslateY(20);
        return vb;
    }

    private TableView<Farmaceutico> grilla() {
        tvFarmaceuticos = new TableView<>();

        TableColumn colId = new TableColumn("Cédula");
        colId.setCellValueFactory(new PropertyValueFactory<>("Cedula"));

        TableColumn colNombre = new TableColumn("Nombre");
        colNombre.setCellValueFactory(new PropertyValueFactory<>("Nombre"));

        TableColumn colApellido = new TableColumn("Apellido");
        colApellido.setCellValueFactory(new PropertyValueFactory<>("Apellido"));

        TableColumn colCorreo = new TableColumn("Correo");
        colCorreo.setCellValueFactory(new PropertyValueFactory<>("Correo"));

        colId.setMinWidth(200);
        colNombre.setMinWidth(200);
        colApellido.setMinWidth(200);
        colCorreo.setMinWidth(200);

        tvFarmaceuticos.getColumns().addAll(colId, colNombre, colApellido, colCorreo);
        tvFarmaceuticos.setMinWidth(ancho - 50);
        tvFarmaceuticos.setMaxWidth(ancho - 50);
        tvFarmaceuticos.setMinHeight(alto / 3);
        tvFarmaceuticos.setTranslateX(20);

        return tvFarmaceuticos;
    }

    protected void cargarGrilla() {

        tvFarmaceuticos.getItems().clear();
        
        try {
            tvFarmaceuticos.getItems().addAll(ConexionBD.obtenerFarmas());
        } catch (SQLException ex) {
            Metodos.mensajealerta(ex.getMessage());
        }
    }
}
