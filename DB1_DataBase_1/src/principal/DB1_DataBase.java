/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import javafx.scene.paint.Color;
import java.io.FileNotFoundException;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author joanp y Domenica Barreiro
 */
public class DB1_DataBase extends Application {
    
    @Override
    public void start(Stage primaryStage) throws FileNotFoundException {
       
        LoginPane lp = new LoginPane(primaryStage);
        //InfoUsuarios.inicializarDatos();
        /*CitaPane cp = new CitaPane();
        NuevoPaciente np = new NuevoPaciente(primaryStage);
        SesionOperador so = new SesionOperador(primaryStage);
        TurnosPane tp = new TurnosPane();
        ModificarPaciente mp = new ModificarPaciente(primaryStage);
        ConsultarPaciente cpa = new ConsultarPaciente(primaryStage);
        ModificarCita mc = new ModificarCita();
        ConsultarCita cc = new ConsultarCita();
        VerificarDisponibilidad vd = new VerificarDisponibilidad();
        NuevaCita nc = new NuevaCita();
        ConsultarMedicamento cm =  new ConsultarMedicamento();
        NuevoMedicamento nm = new NuevoMedicamento();*/

        Scene scene = new Scene(lp.getRoot(), 1000, 500, Color.WHITE);
        
        primaryStage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
        primaryStage.setScene(scene);
        primaryStage.centerOnScreen (); 
        primaryStage.setMaximized(true);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
