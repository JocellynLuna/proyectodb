/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Usuarios;

import java.io.Serializable;

/**
 *
 * @author Domenica Barreiro
 */
public class Usuario implements Serializable {
    
    private String nombre;
    private String apellido;
    private String cedula;
    private String correo;
    private String contrasenia;
    private boolean estado;
    
    /**
     * Contructor del objeto Usuario  
     * @param nombre String con nombre del Usuario
     * @param apellido String con apellido del Usuario
     * @param cedula String con cedula del Usuario
     * @param correo String con correo del Usuario
     * @param contrasenia String con contraseÃ±a del Usuario
     */
    public Usuario(String nombre, String apellido, String cedula, String correo, String contrasenia) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.correo = correo;
        this.contrasenia = contrasenia;
        this.estado = true;
    }
    
    public void desactivarUsuario() {
        this.estado = false;
    }
    
    /**
     * Setter de la variable Nombre
     * @param nombre String 
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Setter de la variable Apellido
     * @param apellido String 
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    /**
     * Setter de la variable cedula
     * @param cedula String 
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
    
    /**
     * Setter de la variable Correo
     * @param correo String 
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * Setter de la variable contraseÃ±a
     * @param contrasenia String 
     */
    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }
    
    /**
     * Getter de la variable nombre
     * @return String nombre
     */
    public String getNombre() {
        return nombre;
    }
    
    /**
     * Getter de la variable apellido
     * @return String apellido
     */
    public String getApellido() {
        return apellido;
    }
    
    /**
     * Getter de la variable cedula
     * @return String cedula
     */
    public String getCedula() {
        return cedula;
    }
    
    /**
     * Getter de la variable correo
     * @return String correo
     */
    public String getCorreo() {
        return correo;
    }
    
    /**
     * Getter de la variable contrasenia
     * @return String contraseÃ±a
     */
    public String getContrasenia() {
        return contrasenia;
    }

    @Override
    public String toString() {
        return nombre;
    }
    
}
