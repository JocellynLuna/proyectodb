/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Operador;

import funcional.Util;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import principal.LoginPane;

/**
 *
 * @author joanp
 */
public final class SesionOperador {
    private final BorderPane root;
    private final Stage stage;
    private final Scene scene;
    
    public SesionOperador(Stage stage) throws FileNotFoundException{        
        root = new BorderPane();
        
        root.setTop(sesionTop());
        root.setCenter(sesionCenter());
        root.setBottom(sesionBottom());
        
        root.setStyle("-fx-background-image: url('"+getClass().getResource("/recursos/fondo_operador.jpg").toExternalForm()+"');"
                                        + "-fx-background-repeat: stretch;"  
                                       + "-fx-background-size: "+stage.getWidth()+" "+stage.getHeight()+";"                
                                        + "-fx-background-position: center;");
        
        this.stage = stage;
        stage.setTitle("Operador");
        scene = new Scene(root,stage.getWidth(), stage.getHeight());
        stage.setScene(scene);
        stage.show();
    }
    
    public AnchorPane sesionTop() throws FileNotFoundException{
        VBox result = top();
        AnchorPane ap = new AnchorPane(result);
        AnchorPane.setLeftAnchor(result, 50.0);
        AnchorPane.setTopAnchor(result, 20.0);
        //AnchorPane.setBottomAnchor(result, 100.0);
        AnchorPane.setRightAnchor(result, 25.0);
        return ap;
    }
    
    public VBox top() throws FileNotFoundException{
        VBox principal = new VBox();
        
        ImageView csesion = Util.imagen("src/recursos/CerrarSesion.png",50,50);
        Button cerrar = new Button("Cerrar Sesión",csesion);
        cerrar.setFont(Font.font("Verdana",15));
        cerrar.setOnAction(new ManejadorCerrar()); /// handle event
          
        HBox boton = new HBox(cerrar);
        boton.setAlignment(Pos.CENTER_RIGHT);
        
        ImageView logo = Util.imagen("src/recursos/logo.PNG",500,275);
        
        ImageView operador = Util.imagen("src/recursos/operador.png",250,250);
        Text detalle = Util.styleText("Hola, NombreOperador", "Verdana", 25,false);
        StackPane pane = new StackPane();
        pane.getChildren().addAll(operador,detalle);
        pane.setAlignment(Pos.BOTTOM_CENTER);
        
        HBox general = new HBox(logo,pane);
        general.setAlignment(Pos.CENTER);
        general.setSpacing(800);
        
        principal.getChildren().addAll(boton,general);
        principal.setPrefHeight(375);
        return principal;
    }
    
    public VBox sesionCenter() throws FileNotFoundException{
        Text titulo = Util.styleText("Sistema de Gestión del Operador de Call Center","Verdana", 60,true);
        
        ImageView paciente = Util.imagen("src/recursos/paciente.png",100, 100);
        Text pac = Util.styleText("Paciente", "Verdana", 45, true);
        HBox pacient = new HBox(paciente,pac);
        pacient.setSpacing(10);
        
        ImageView cita = Util.imagen("src/recursos/cita.png",100, 100);
        Text cit = Util.styleText("Cita", "Verdana", 45, true);
        HBox ct = new HBox(cita,cit);
        ct.setSpacing(10);
        
        HBox hb = new HBox(pacient,ct);
        hb.setSpacing(150); hb.setAlignment(Pos.CENTER);
        VBox principal = new VBox(titulo,hb);
        principal.setPrefHeight(200); 
        principal.setSpacing(50);
        principal.setAlignment(Pos.CENTER);
        
        return principal;
    }
    
    public AnchorPane sesionBottom() throws FileNotFoundException{
        GridPane result = grid();
        
        AnchorPane ap = new AnchorPane(result);
        AnchorPane.setLeftAnchor(result, 50.0);
        AnchorPane.setTopAnchor(result, 0.0);
        AnchorPane.setBottomAnchor(result, 100.0);
        AnchorPane.setRightAnchor(result, 50.0);
        
        return ap;
    }

    public GridPane grid() throws FileNotFoundException{
        Button nuevop = Util.botonImagen("   Nuevo paciente     ", "src/recursos/nuevo.png",50,50);
        nuevop.setOnAction(new ManejadorNuevoP()); ///manejador
        Button modifp = Util.botonImagen("   Modificar paciente ","src/recursos/modificar.png",50,50);
        modifp.setOnAction(new ManejadorModificarP()); ///manejador
        Button consp = Util.botonImagen("   Consultar paciente","src/recursos/buscar.png",50,50);
        consp.setOnAction(new ManejadorConsultarP()); ///manejador
        Button nuevoc = Util.botonImagen("   Nueva cita     ","src/recursos/nuevo.png",50,50);
        nuevoc.setOnAction(new ManejadorNuevoC()); ///manejador
        Button modifc = Util.botonImagen("   Modificar cita ","src/recursos/modificar.png",50,50);
        modifc.setOnAction(new ManejadorModificarC()); ///manejador
        Button consc = Util.botonImagen("   Consultar cita","src/recursos/buscar.png",50,50);
        consc.setOnAction(new ManejadorConsultarC());
        
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10)); 
        gridPane.setVgap(40); 
        gridPane.setHgap(70);
        
        gridPane.add(nuevop, 0, 0); 
        gridPane.add(nuevoc, 1, 0); 
        gridPane.add(modifp, 0, 1);       
        gridPane.add(modifc, 1, 1); 
        gridPane.add(consp, 0, 2); 
        gridPane.add(consc, 1, 2); 
        gridPane.setAlignment(Pos.CENTER);
        
        gridPane.setPrefHeight(400);
        return gridPane;
    }
    public BorderPane getRoot() {
        return root;
    }

    public Stage getStage() {
        return stage;
    }
   
    private class ManejadorCerrar implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                LoginPane lp = new LoginPane(stage);
                Scene scene = new Scene(lp.getRoot(), stage.getWidth(), stage.getHeight(), Color.WHITE);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(scene);
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
            }
        } 
    }
    
    private class ManejadorNuevoP implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                NuevoPaciente np = new NuevoPaciente(stage);
                Scene scene = new Scene(np.getRoot(), stage.getWidth(), stage.getHeight(), Color.WHITE);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(scene);
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SesionOperador.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } 
    }
    
    private class ManejadorModificarP implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                ModificarPaciente mp = new ModificarPaciente(stage);
                Scene scene = new Scene(mp.getRoot(), stage.getWidth(), stage.getHeight(), Color.WHITE);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(scene);
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SesionOperador.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } 
    }
    
    private class ManejadorConsultarP implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                ConsultarPaciente mp = new ConsultarPaciente(stage);
                Scene scene = new Scene(mp.getRoot(), stage.getWidth(), stage.getHeight(), Color.WHITE);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(scene);
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SesionOperador.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } 
    }
    
    private class ManejadorNuevoC implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                NuevaCita nc = new NuevaCita(stage);
                Scene scene = new Scene(nc.getRoot(), stage.getWidth(), stage.getHeight(), Color.WHITE);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(scene);
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SesionOperador.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } 
    }
    
    private class ManejadorModificarC implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                ModificarCita mc = new ModificarCita(stage);
                Scene scene = new Scene(mc.getRoot(), stage.getWidth(), stage.getHeight(), Color.WHITE);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(scene);
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SesionOperador.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } 
    }
    
    private class ManejadorConsultarC implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                ConsultarCita cc = new ConsultarCita (stage);
                Scene scene = new Scene(cc.getRoot(), stage.getWidth(), stage.getHeight(), Color.WHITE);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(scene);
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SesionOperador.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } 
    }

    public Scene getScene() {
        return scene;
    }
    
}