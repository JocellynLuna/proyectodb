/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Doctor;

import BaseDeDatosConexion.ConexionBD;
import Datos.Metodos;
import Datos.Validaciones;
import java.sql.SQLException;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Doménica Barreiro
 */
public class NuevaReceta {
    
    private TextField cedula_Paciente;
    private TextField cedula_Doctor;
    private TextField id_medicina;
    private TextArea frecuencia;
    private TextArea dosis;
    private VBox root;
    private final Stage main;
    
    public NuevaReceta (Stage principal) {
        
        main = new Stage();
        main.initModality(Modality.WINDOW_MODAL);
        main.initOwner(principal);
        
        main.setTitle("Nueva Receta");
        
        root = new VBox();
        
        root.setAlignment(Pos.CENTER);
        root.setPrefWidth(550);
        root.setPrefHeight(550);
        
        root.setPrefSize(550,550);
        root.setSpacing(15);
        root.setPadding(new Insets(10, 10, 20, 10));
        root.getChildren().addAll(CrearFormulario());
        root.setStyle("-fx-background-color: #ffffff;");
        Scene scene = new Scene(root);
        main.setScene(scene);
        
    }
    
    public Stage getNuevaReceta() {
        return main;
    }
    
    
    private VBox CrearFormulario() {
        
        GridPane gpDatos = new GridPane();
        gpDatos.setAlignment(Pos.CENTER);
        gpDatos.setHgap(2);
        gpDatos.setVgap(15);
        
        //TITLES------------------------------------------------
        Label title = new Label("Crear Receta");
        Label ind = new Label("Ingrese los siguientes datos");
        title.setStyle("-fx-font-weight: bold;-fx-font-size: 20pt");
        ind.setStyle("-fx-font-weight: bold;-fx-font-size: 14pt");
        VBox vb = new VBox(title, ind);
        vb.setAlignment(Pos.CENTER_LEFT);
        vb.setSpacing(10);
        
        //LABELS-------------------------------------------------
        gpDatos.add(new Label("Cédula del Paciente"),0,1);
        GridPane.setHalignment(new Label("Cédula del Paciente"), HPos.CENTER);
        gpDatos.add(new Label("Cédula del Doctor"),0,2);
        gpDatos.add(new Label("Id_Medicamento"),0,3);
        gpDatos.add(new Label("Frecuencia"),0,4);
        gpDatos.add(new Label("Dosis"),0,5);
        
        //TEXTBOXS-----------------------------------------------
        cedula_Paciente = new TextField();
        cedula_Doctor = new TextField();
        id_medicina = new TextField();
        frecuencia = new TextArea();
        frecuencia.setPrefSize(350, 100);
        frecuencia.autosize();
        dosis = new TextArea();
        dosis.setPrefSize(350, 100);
        dosis.autosize();
        
        
        gpDatos.add(cedula_Paciente,1,1);
        gpDatos.add(cedula_Doctor,1,2);
        gpDatos.add(id_medicina,1,3);
        gpDatos.add(frecuencia,1,4);
        gpDatos.add(dosis,1,5);
        
        //Botón-----------------
        Button btnGuardar = new Button("     Guardar     ");
                
        accionGuardar(btnGuardar, gpDatos);
        
        Button btncancelar = new Button("     Cancelar     ");
        btncancelar.setOnAction((e)->{
            main.close();
        }); 
        
        HBox hbG = new HBox(btnGuardar,btncancelar);
        hbG.setAlignment(Pos.CENTER);
        hbG.setSpacing(20);
        
        
        //Añadir ---------------------------------------------------------------------------------------------
        
        VBox vbDerecha = new VBox(vb,gpDatos,hbG);
        vbDerecha.setSpacing(20);
        vbDerecha.setAlignment(Pos.CENTER);
        
        return vbDerecha;
        
    }
    
    private void accionGuardar(Button btnGuardar, GridPane gpDatos){
        btnGuardar.setOnAction((e) -> {

            if (Validaciones.validarTextosPane(gpDatos)) {
                Metodos.mensajealerta("Debe llenar todos los campos antes de continuar.");
            } else if (validarDatos()) {
                String id_Paciente = cedula_Paciente.getText();
                String id_Doctor = cedula_Doctor.getText();
                String medicina = id_medicina.getText();
                String fr = frecuencia.getText();
                String dos = dosis.getText();


                try {
                    ConexionBD.insertarReceta(id_Paciente,id_Doctor,medicina,fr,dos);
                    
                    Metodos.mensajealerta("Se ha registrado la información correctamente!");
                    Metodos.vaciarTextos(gpDatos);
                    
                } catch (SQLException ex) {
                    Metodos.mensajealerta("Ocurrió un error. " + ex.getMessage());
                }
            }
        });
    }
    
    
    private boolean validarDatos(){
        return Validaciones.validarNumId(cedula_Paciente)&&
               Validaciones.validarNumId(cedula_Doctor)&&
               Validaciones.validarTexto(id_medicina)&&
               Validaciones.validarTexto(frecuencia)&&
               Validaciones.validarTexto(dosis);
    }
    
}

