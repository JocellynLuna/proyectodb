/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Administrador;

import BaseDeDatosConexion.ConexionBD;
import Datos.Metodos;
import Modelo.Usuarios.POperador.Operador;
import funcional.Util;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Domenica Barreiro
 */
public class Operadores {
    
    private TableView<Operador> tvOperadores;
    private double ancho;
    private double alto;
    private Tab Operadores;
    private Stage origen;

    public Operadores (Stage stage) throws FileNotFoundException {
        
        this.origen = stage;
        ancho = stage.getWidth();
        alto = stage.getHeight();
        
        Operadores = new Tab("Operadores                 ");
        VBox vb = new VBox(arriba(),grilla());
        cargarGrilla();
        vb.setSpacing(20);
        vb.setStyle("-fx-background-image: url('"+getClass().getResource("/recursos/fondo_operador.jpg").toExternalForm()+"');"
                                        + "-fx-background-repeat: stretch;"  
                                       + "-fx-background-size: "+ancho+" "+alto+";"                
                                        + "-fx-background-position: center;");
        
        
        Operadores.setContent(vb);
        Operadores.setClosable(false);
    }

    public Tab getOperadores() {
        return Operadores;
    }
    
    private VBox arriba() throws FileNotFoundException{
        
        ImageView im = Util.imagen("src/recursos/logo.png", 200, 100);
        VBox logo = new VBox(im);
        
        Button btnNuevo = new Button("Nuevo Operador");
        btnNuevo.setOnAction(e -> {
            NuevoOperador pn = new NuevoOperador(origen, this);
            Stage main = pn.getNuevoOperador();
            main.show();
        });
        
        VBox vb = new VBox(logo,btnNuevo);
        vb.setSpacing(10);
        vb.setPadding(new Insets(0,0,20,0));
        vb.setTranslateX(20);
        vb.setTranslateY(20);
        return vb;
    }
    
    private TableView<Operador> grilla(){
        tvOperadores = new TableView<>();
        
        TableColumn colId = new TableColumn("Cédula");
        colId.setCellValueFactory(new PropertyValueFactory<>("Cedula"));
        
        TableColumn colNombre = new TableColumn("Nombre");
        colNombre.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
        
        TableColumn colApellido = new TableColumn("Apellido");
        colApellido.setCellValueFactory(new PropertyValueFactory<>("Apellido"));
        
        TableColumn colCorreo = new TableColumn("Correo");
        colCorreo.setCellValueFactory(new PropertyValueFactory<>("Correo"));
        
        colId.setMinWidth(200);
        colNombre.setMinWidth(200);
        colApellido.setMinWidth(200);
        colCorreo.setMinWidth(200);
        
        
        tvOperadores.getColumns().addAll(colId,colNombre,colApellido,colCorreo);
        tvOperadores.setMinWidth(ancho-50);
        tvOperadores.setMaxWidth(ancho-50);
        tvOperadores.setMinHeight(alto/3);
        tvOperadores.setTranslateX(20);
        
        return tvOperadores;
    }
    
    protected void cargarGrilla(){
        
        tvOperadores.getItems().clear();
        
        try {
            tvOperadores.getItems().addAll(ConexionBD.ObtenerOperadores());
        } catch (SQLException ex) {
            Metodos.mensajealerta(ex.getMessage());
        }
        
    }
    
}
