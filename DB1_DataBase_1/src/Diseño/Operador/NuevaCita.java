/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Operador;

import BaseDeDatosConexion.ConexionBD;
import Modelo.Usuarios.POperador.Cita;
import funcional.Util;
import static funcional.Util.styleText;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author joanp
 */
public final class NuevaCita {
    private final BorderPane root;
    private final Stage stage;
    private Cita cita;
    private TextField cedula;
    private TextField paciente;
    private TextField idDoc;
    private TextField doctor;
    private TextField idCita;
    private TextField fecha;
    private TextField hora;
    private TextField color;

    public Cita getCita() {
        return cita;
    }

    public void setCita(Cita cita) {
        this.cita = cita;
    }
    
    public NuevaCita(Stage stage) throws FileNotFoundException{
        root = new BorderPane();
        this.stage = stage;
        cita = null;
        cita = ModificarCita.getcita();
        
        root.setTop(Util.operadorTop());
        root.setCenter(operadorCenter());
        root.setBottom(citaBottom());
        llenarText();
        
        root.setStyle("-fx-background-image: url('"+getClass().getResource("/recursos/fondo_cita.jpg").toExternalForm()+"');"
                                        + "-fx-background-repeat: stretch;"  
                                       + "-fx-background-size: "+stage.getWidth()+" "+stage.getHeight()+";"                
                                        + "-fx-background-position: center;");
    }
    
    public AnchorPane operadorCenter(){
        AnchorPane ap = new AnchorPane();
        
        Text text = styleText("Nueva Cita", "Verdana", 60,true);
        Text detail = styleText("Ingrese los datos descritos para la asignación de la cita. "
                + "\nAntes de asignar a un doctor verifique su disponibilidad con el horario.", "Verdana", 30, true);
        
        VBox result = new VBox(text,detail);
        result.setAlignment(Pos.CENTER_LEFT);
        result.setSpacing(30);
        
        ap.getChildren().add(result);
        AnchorPane.setLeftAnchor(result, 100.0);
        AnchorPane.setTopAnchor(result, 25.0);
                
        return ap;
    }
    
    public VBox citaBottom() throws FileNotFoundException{
        GridPane gridPane = datos();  
        
        Button guardar = Util.botonImagen("  Guardar ","src/recursos/guardar.png", 50, 50);
        guardar.setPrefHeight(95);
        Button regresar = Util.botonImagen("  Regresar","src/recursos/regresar.png", 50, 50);
        regresar.setPrefHeight(95);
        regresar.setOnAction(new ManejadorRegresar());  ///manejador
        Button disponible = Util.botonImagen("      Verificar\n  Disponibilidad","src/recursos/alerta.png", 50, 50);
        disponible.setOnAction(new ManejadorDisponibilidad()); /// manejador
        
        HBox botones = new HBox(disponible,guardar, regresar);
        botones.setSpacing(50);
        botones.setAlignment(Pos.CENTER);
        
        VBox result = new VBox(gridPane,botones);
        result.setSpacing(30);
        gridPane.setAlignment(Pos.CENTER);
        result.setPrefHeight(400);

        return result;
    }
    
    public GridPane datos(){
        GridPane gridPane = new GridPane();
        
        cedula = Util.sizeTextField(525,40);
        paciente = Util.sizeTextField(525,40);
        idDoc = Util.sizeTextField(525,40);
        doctor = Util.sizeTextField(525,40);
        idCita =Util.sizeTextField(525,40);
        fecha = Util.sizeTextField(525,40);
        hora = Util.sizeTextField(525,40);
        color = Util.sizeTextField(525,40);
        
        //cedula.setText(value);
        
        gridPane.setVgap(20); 
        gridPane.setHgap(100);
        gridPane.add(Util.styleText("Cédula","Verdana",30,false), 0, 0); 
        gridPane.add(cedula, 1, 0); 
        gridPane.add(Util.styleText("Paciente","Verdana",30,false), 0, 1);       
        gridPane.add(paciente, 1, 1); 
        gridPane.add(Util.styleText("ID Doctor","Verdana",30,false), 0, 2); 
        gridPane.add(idDoc, 1, 2);
        gridPane.add(Util.styleText("Doctor","Verdana",30,false), 0, 3); 
        gridPane.add(doctor, 1, 3);
        gridPane.add(Util.styleText("ID Cita","Verdana",30,false), 2, 0); 
        gridPane.add(idCita, 3, 0);
        gridPane.add(Util.styleText("Fecha","Verdana",30,false), 2, 1); 
        gridPane.add(fecha, 3, 1);
        gridPane.add(Util.styleText("Hora","Verdana",30,false), 2, 2); 
        gridPane.add(hora, 3, 2);
        gridPane.add(Util.styleText("Color","Verdana",30,false), 2, 3); 
        gridPane.add(color, 3, 3);
        
        return gridPane;
    }

    public BorderPane getRoot() {
        return root;
    }
    
    private class ManejadorRegresar implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                SesionOperador sop = new SesionOperador(stage);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(sop.getScene());
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
            }
        } 
    }
    
    private class ManejadorDisponibilidad implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                VerificarDisponibilidad vd = new VerificarDisponibilidad(stage);
                Scene scene = new Scene(vd.getRoot(),stage.getWidth(),stage.getHeight(), Color.WHITE);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(scene);
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
            }
        } 
    }
    
    private void llenarText(){
        
        if(cita!=null){
            cedula.setText(ModificarCita.citaModific.getCedulaP());
            cedula.setDisable(true);
            
            paciente.setText(cita.getNombreP());
            paciente.setDisable(true);
            
            doctor.setText(cita.getNombreD());
            doctor.setDisable(true);
            
            idDoc.setText(cita.getCedulaD());
            
            fecha.setText(cita.getFecha().toString());
            
            hora.setText(cita.getHora().toString());
            
            color.setText(cita.getColorN());
        }
    }
    
    public void comparar(){        
        if(!idDoc.getText().equals(cita.getCedulaD())){
            cita.setCedulaD(idDoc.getText());
        }
        
        if(!fecha.getText().equals(cita.getFecha().toString())){
            cita.setFecha(LocalDate.parse(fecha.getText()));
        }
        
        if(!hora.getText().equals(cita.getHora().toString())){
            cita.setHora(LocalTime.parse(idDoc.getText()));
        }
        
        if(!color.getText().equals(cita.getColorN())){
            cita.setCedulaD(color.getText());
        }
    }

    public void setCedula(TextField cedula) throws SQLException {
        
        this.cedula = cedula;
    }

    public void setPaciente(TextField paciente) {
        this.paciente = paciente;
    }

    public void setIdDoc(TextField idDoc) throws SQLException {
        String id = cita.getIdCita();
        ConexionBD.hacerQuery("UPDATE Cita SET Id_Doctor = idDoctor WHERE Id_Cita = id");
        this.idDoc = idDoc;
    }

    public void setDoctor(TextField doctor) {
        this.doctor = doctor;
    }

    public void setIdCita(TextField idCita) {
        this.idCita = idCita;
    }

    public void setFecha(TextField fecha) throws SQLException {
        String id = cita.getIdCita();
        ConexionBD.hacerQuery("UPDATE Cita SET Fecha = fecha WHERE Id_Cita = id");
        this.fecha = fecha;
    }

    public void setHora(TextField hora) throws SQLException {
        String id = cita.getIdCita();
        ConexionBD.hacerQuery("UPDATE Cita SET Hora = hora WHERE Id_Cita = id");
        this.hora = hora;
    }

    public void setColor(TextField color) throws SQLException {
        String id = cita.getIdCita();
        ConexionBD.hacerQuery("UPDATE Cita SET Color = color WHERE Id_Cita = id");
        this.color = color;
    }
    
    

    public Stage getStage() {
        return stage;
    }

    public TextField getCedula() {
        return cedula;
    }

    public TextField getPaciente() {
        return paciente;
    }

    public TextField getIdDoc() {
        return idDoc;
    }

    public TextField getDoctor() {
        return doctor;
    }

    public TextField getIdCita() {
        return idCita;
    }

    public TextField getFecha() {
        return fecha;
    }

    public TextField getHora() {
        return hora;
    }

    public TextField getColor() {
        return color;
    }

    
    
}
