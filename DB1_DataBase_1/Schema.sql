drop database if exists Centro_Dermatologico;
CREATE DATABASE Centro_Dermatologico;
USE Centro_Dermatologico;

CREATE TABLE Paciente (
	Cedula CHAR(10) PRIMARY KEY,
	Nombre VARCHAR(20) NOT NULL,
	Apellido VARCHAR(20) NOT NULL,
	Direccion VARCHAR(30) NOT NULL,
	Telefono CHAR(10) NOT NULL
);

CREATE TABLE Proveedor (
	RUC CHAR(13) PRIMARY KEY,
	Nombre VARCHAR(10) NOT NULL,
	Apellido VARCHAR(10) NOT NULL,
	RazonSocial VARCHAR(10) NOT NULL
);

CREATE TABLE Medicamentos (
	Id_Medicamento int auto_increment PRIMARY KEY,
	Nombre VARCHAR(15) NOT NULL,
	Marca CHAR(13) NOT NULL,
	Gramaje VARCHAR(10) NOT NULL,
    FOREIGN KEY (Marca) REFERENCES Proveedor(RUC)
    );

CREATE TABLE Patologias (
	Id_Patologia int auto_increment PRIMARY KEY,
	Nombre VARCHAR(10) NOT NULL,
	Descripcion VARCHAR(30) NOT NULL
);

CREATE TABLE Historial_Medico (
	Id_Paciente VARCHAR(10),
	Id_Patologia int,
    observaciones VARCHAR(200),
    PRIMARY KEY (Id_Paciente, Id_Patologia),
	FOREIGN KEY (Id_Paciente) REFERENCES Paciente(Cedula),
	FOREIGN KEY (Id_Patologia) REFERENCES Patologias(Id_Patologia)
);

CREATE TABLE Usuario (
    contrasena Varchar(20) not null,
    cedula CHAR(10) PRIMARY KEY,
    tipo int not null,
    activo boolean not null,
    index in_ced (cedula));
    
CREATE TABLE Operador (
	Cedula CHAR(10) PRIMARY KEY,
	Nombre VARCHAR(20) NOT NULL,
	Apellido VARCHAR(20) NOT NULL,
    correo Varchar(30) NOT NULL,
    FOREIGN KEY(Cedula) REFERENCES Usuario(cedula));
    
CREATE TABLE Farmaceutico (
	Cedula CHAR(10) PRIMARY KEY,
	Nombre VARCHAR(20) NOT NULL,
	Apellido VARCHAR(20) NOT NULL,
    correo Varchar(30) NOT NULL,
    FOREIGN KEY(cedula) REFERENCES Usuario(cedula));

CREATE TABLE Doctor (
	Cedula CHAR(10) PRIMARY KEY,
	Nombre VARCHAR(20) NOT NULL,
	Apellido VARCHAR(20) NOT NULL,
    correo Varchar(30) NOT NULL,
	Telefono CHAR(10) NOT NULL,
	Fecha_Ingreso DATE NOT NULL,
	Hora_Ingreso TIME NOT NULL DEFAULT '08:00',
	Hora_Salida TIME NOT NULL DEFAULT '17:00',
	Especialidad VARCHAR(20) NOT NULL,
	Cargo VARCHAR(15) NOT NULL,
    FOREIGN KEY(cedula) REFERENCES Usuario(cedula));

CREATE TABLE Receta (
	Id_Receta VARCHAR(10) PRIMARY KEY,
	Id_Paciente VARCHAR(10) NOT NULL,
	Id_Doctor VARCHAR(10) NOT NULL,
	Id_Medicamento int NOT NULL,
	Frecuencia VARCHAR(10) NOT NULL,
	Dosis VARCHAR(10) NOT NULL,
	FOREIGN KEY (Id_Paciente) REFERENCES Paciente (Cedula),
	FOREIGN KEY (Id_Doctor) REFERENCES Doctor (cedula),
	FOREIGN KEY (Id_Medicamento) REFERENCES Medicamentos(Id_Medicamento)
);

CREATE TABLE Cita (
	Id_Cita int auto_increment PRIMARY KEY,
	Id_Paciente VARCHAR(10) NOT NULL,
	Id_Doctor VARCHAR(10) NOT NULL,
	Fecha DATE NOT NULL,
	Hora TIME NOT NULL,
	Color VARCHAR(10) NOT NULL,
    activo boolean,
	FOREIGN KEY (Id_Paciente) REFERENCES Paciente (Cedula),
	FOREIGN KEY (Id_Doctor) REFERENCES Doctor (cedula)
);

#								Triggers
/*Delimiter $
Create trigger tPrivilgioDoctor After Insert On Doctor
For Each Row
	Begin

	End$
Delimiter ;
*/
#								Stored Procedures

drop Procedure if exists  insertDoctor;
Delimiter $
Create Procedure insertDoctor (cedula char(10), nombre varchar(20), apellido  VARCHAR(20), telefono CHAR(10),
								correo VARCHAR(30), contrasena Varchar(20),fechaIngreso DATE, horaIngreso TIME,
                                horaSalida TIME, especialidad VARCHAR(20), cargo VARCHAR(15)) 
	Begin
    Start transaction;
    
    If	exists (Select u.Cedula From usuario u Where u.Cedula = cedula)  Or
		exists (Select d.Cedula From doctor d Where d.Cedula = cedula) Then
        drop user cedula;
		signal sqlState '45000'
		Set message_text = 'Cédula existente';
	End If;
    
    Insert Into usuario values(contrasena, cedula, 2, true);
    
    Insert Into doctor values(cedula, nombre, apellido,correo, telefono, fechaIngreso, horaIngreso,
			horaSalida, especialidad, cargo);
            
	Grant Select, insert On centro_dermatologico.hiinsertHistorialstorial_medico To cedula;
	Grant Select On centro_dermatologico.paciente To cedula;
	Grant Select On centro_dermatologico.patologias To cedula;
	Grant Select On centro_dermatologico.receta To cedula;
	Grant execute On procedure centro_dermatologico.buscarPacienteDoc To cedula;
    commit;
	End$
Delimiter ;


drop Procedure if exists  insertFarmaceutico;
Delimiter $
Create Procedure insertFarmaceutico (cedula char(10), nombre varchar(20), apellido  VARCHAR(20),
									correo VARCHAR(30), contrasena Varchar(20)) 
	Begin
    Start transaction;
    If	exists (Select u.Cedula From usuario u Where u.Cedula = cedula)  Or
		exists (Select f.Cedula From farmaceutico f Where f.Cedula = cedula) Then
        drop user cedula;
		signal sqlState '45000'
		Set message_text = 'cédula ya existente';
	End If;
    
    Insert Into farmaceutico values(cedula, nombre,correo , apellido);
	
    Insert Into usuario values(constrasena, cedula, 1, true);
	commit;
	End$
Delimiter ;


drop Procedure if exists  insertOperador;
Delimiter $
Create Procedure insertOperador (cedula char(10), nombre varchar(20), apellido  VARCHAR(20),
									correo VARCHAR(30), contrasena Varchar(20)) 
	Begin
    Start transaction;
    If	exists (Select u.Cedula From usuario u Where u.Cedula = cedula)  Or
		exists (Select o.Cedula From operador o Where o.Cedula = cedula) Then
        drop user cedula;
		signal sqlState '45000'
		Set message_text = 'cédula ya existente';
	End If;
    
    Insert Into Operador values(cedula, nombre, apellido, correo);
	
    Insert Into usuario values(constrasena, cedula, 3, true);
	commit;
	End$
Delimiter ;


drop Procedure if exists  buscarPacienteDoc;
Delimiter $
Create Procedure buscarPacienteDoc (cedulaDoc char(10)) 
	Begin
    Select 	p.*, c.Fecha, c.Hora, c.Color
    From	cita c
    Join	paciente p On p.cedula = c.id_paciente
    Where	c.id_doctor = cedulaDoc
			And c.activo = true;
	End$
Delimiter ;



drop Procedure if exists  buscarHistorialPac;
Delimiter $
Create Procedure buscarHistorialPac (cedulaPac char(10))   
	Begin     
	Select  p.*, pt.nombre as nombre_pat,pt.descripcion, hm.observaciones     
	From    historial_medico hm     
	Join    paciente p On p.Cedula = hm.Id_Paciente     
	Join    patologias pt On pt.Id_Patologia = hm.Id_Patologia     
	Where   p.cedula = cedulaPac;  
	End$
Delimiter ;

Flush privileges;

#										Views
Create view reporteDoc As
Select	d.*
From	Doctor d
Join	Usuario u On u.cedula = d.cedula
Where	u.activo = true;

Create view reporteFarma As
Select	f.*
From	Farmaceutico f
Join	Usuario u On u.cedula = f.cedula
Where	u.activo = true;

Create view reporteOpe As
Select	p.*
From	Operador p
Join	Usuario u On u.cedula = p.cedula
Where	u.activo = true;

Create view reportePacientes As
Select	p.*
From	Paciente p;

Drop view reporteCitas;
Create view reporteCitas As
Select	c.*, d.nombre as nombreD, d.apellido AS apellidoD, d.cedula As cedulaD
			, p.nombre as nombreP, p.apellido AS apellidoP, p.cedula As cedulaP
From	Cita c
Join	Doctor d	On d.cedula = c.id_doctor
Join	Paciente p On c.id_paciente = p.cedula;

#								Roles y Privilegios

-- InicioSesion
Create User InicioSesion Identified By 'bAs3DAT0s';
Grant select On centro_dermatologico.usuario To InicioSesion;
Grant select On centro_dermatologico.doctor To InicioSesion;
Grant select On centro_dermatologico.farmaceutico To InicioSesion;
Grant select On centro_dermatologico.operador To InicioSesion;
-- Admin (0)
Create User '0987654321' Identified by 'admin';
Grant execute On procedure centro_dermatologico.insertFarmaceutico To '0987654321';
Grant execute On procedure centro_dermatologico.insertDoctor To '0987654321';
Grant select On centro_dermatologico.usuario To '0987654321';
Grant select On centro_dermatologico.doctor To '0987654321';
Grant select On centro_dermatologico.farmaceutico To '0987654321';
Grant select On centro_dermatologico.operador To '0987654321';
Grant show view On centro_dermatologico.* To '0987654321';
Grant grant option On  centro_dermatologico.* To '0987654321';

Drop user '0362121334'@'%','0915478583'@'%','0916542343'@'%','0934323547'@'%','0945671231'@'%',
			'0912312353'@'%','0912656422'@'%','0924573812'@'%','0929538361'@'%','0932423423'@'%',
			'0524533463'@'%','0816475635'@'%','0916354822'@'%','0917563545'@'%','0987767616'@'%';

Create User '0362121334'@'%'identified by '123','0915478583'@'%'identified by '123','0916542343'@'%'identified by '123','0934323547'@'%'identified by '123','0945671231'@'%'identified by '123',
			'0912312353'@'%'identified by '123','0912656422'@'%'identified by '123','0924573812'@'%'identified by '123','0929538361'@'%'identified by '123','0932423423'@'%'identified by '123',
			'0524533463'@'%'identified by '123','0816475635'@'%'identified by '123','0916354822'@'%'identified by '123','0917563545'@'%' identified by '123','0987767616'@'%' identified by '123';

Grant all privileges On centro_dermatologico.* To '0362121334'@'%','0915478583'@'%','0916542343'@'%','0934323547'@'%','0945671231'@'%',
			'0912312353'@'%','0912656422'@'%','0924573812'@'%','0929538361'@'%','0932423423'@'%',
			'0524533463'@'%','0816475635'@'%','0916354822'@'%','0917563545'@'%','0987767616'@'%';
            
Flush privileges;

/*
'0362121334'@'%','0915478583'@'%','0916542343'@'%','0934323547'@'%','0945671231'@'%',
			'0912312353'@'%','0912656422'@'%','0924573812'@'%','0929538361'@'%','0932423423'@'%',
			'0524533463'@'%','0816475635'@'%','0916354822'@'%','0917563545'@'%','0987767616'@'%'
*/
-- Farmaceutico (1)
#Grant On centro_dermatologico. To `0362121334`,`0915478583`,`0916542343`,`0934323547`,`0945671231`;

-- Doctor (2)
Grant Select, insert On centro_dermatologico.historial_medico To `0912312353`,`0912656422`,`0924573812`,`0929538361`,`0932423423`;
Grant Select On centro_dermatologico.paciente To `0912312353`,`0912656422`,`0924573812`,`0929538361`,`0932423423`;
Grant Select On centro_dermatologico.patologias To `0912312353`,`0912656422`,`0924573812`,`0929538361`,`0932423423`;
Grant Select On centro_dermatologico.receta To `0912312353`,`0912656422`,`0924573812`,`0929538361`,`0932423423`;
Grant execute On procedure centro_dermatologico.buscarPacienteDoc To `0912312353`,`0912656422`,`0924573812`,`0929538361`,`0932423423`;

Flush privileges;

-- Operador (3)
#Grant  On centro_dermatologico. To `0524533463`,`0816475635`,`0916354822`,`0917563545`,`0987767616`;

Flush privileges;

call buscarPacienteDoc('0929538361');