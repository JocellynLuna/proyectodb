Use centro_dermatologico;

#PROVEEDOR 
INSERT INTO `proveedor` (`RUC`, `Nombre`, `Apellido`, `RazonSocial`) VALUES
 ('1798288377001', 'Elena', 'Acosta', 'Difare')
,('1698234354001', 'Victor', 'Estrada', 'New Select')
,('1798288234001', 'Oscar', 'Matamoros', 'Disfarmur')
,('1838584687001', 'Manuel', 'Mendoza', 'Kronos')
,('1558282347001', 'Sofia', 'Alcivar', 'FarmaSer');

#MEDICAMENTOS
INSERT INTO `medicamentos` (`Nombre`, `Marca`, `Gramaje`) VALUES
 ('Paracetamol', '1558282347001', '100 mg')
,('Ibuprofeno', '1798288377001', '1 g')
,('acetaminofen', '1838584687001', '500 mg')
,('Virlix', '1798288234001', '100 mg')
,('Allegra', '1698234354001', '40 mg');

#USUARIO
INSERT INTO `usuario` (`contrasena`, `cedula`, `tipo`, `activo`)  VALUES
 ('admin', '0987654321', 0, true) #Admin
,('123', '0916542343', 1, true) #Farmaceuticos
,('123', '0915478583', 1, true)
,('123', '0362121334', 1, true)
,('123', '0934323547', 1, true)
,('123', '0945671231', 1, true)
,('123', '0929538361', 2, true) #doctores
,('123', '0932423423', 2, true)
,('123', '0924573812', 2, true)
,('123', '0912312353', 2, true)
,('123', '0912656422', 2, true)
,('123', '0524533463', 3, true) #Call center
,('123', '0916354822', 3, true)
,('123', '0816475635', 3, true)
,('123', '0917563545', 3, true)
,('123', '0987767616', 3, true);

#FARMACEUTICO
INSERT INTO `farmaceutico` (`Cedula`, `Nombre`, `Apellido`,`correo`) VALUES
 ('0916542343','Julissa','Rodriguez','farma1@hotmail.com')
,('0915478583','Daniel','Benalcazar','farma2@hotmail.com')
,('0362121334','Elisa','Olloa','farma3@hotmail.com')
,('0934323547','Jose','Menoscal','farma4@hotmail.com')
,('0945671231','Melanie','Solorzano','farma5@hotmail.com');

#DOCTOR
INSERT INTO `doctor` (`Cedula`, `Nombre`, `Apellido`,`correo`, `Telefono`, `Fecha_Ingreso`, `Hora_Ingreso`, `Hora_Salida`, `Especialidad`, `Cargo`)  VALUES
 ('0929538361', 'Jocellyn ', 'Luna', 'jmluna@espol.edu.ec','0995270940', '2018-12_10', '09:30:00', '14:30:00', 'Dermatólogo', 'Dermatología')
,('0924573812', 'Paulette', 'Vazquez', 'jpvazque@espol.edu.ec', '0983423423', '2018-12_10', '11:30:00', '16:30:00', 'Cosmetólogo', 'Med Estética')
,('0932423423', 'Domenica', 'Barreiro', 'dombpala@espol.edu.ec', '0981121231', '2018-12_10', '09:30:00', '15:30:00', 'Nutricionista', 'Nutrición')
,('0912312353', 'Chrystian', 'Muzzio', 'csmuzzio@espol.edu.ec', '0992131202', '2018-12_10', '13:30:00', '16:30:00', 'Dermatólogo', 'Dermatología')
,('0912656422', 'Ramiro', 'Arteaga', 'ramiroAr@espol.edu.ec', '0992312312', '2018-12_10', '09:30:00', '14:30:00', 'Cosmetólogo', 'Estética Facial');

#OPERADOR
INSERT INTO `operador` (`Cedula`, `Nombre`, `Apellido`,`correo`) VALUES
 ('0524533463','Lucia','Parrales','operador1@hotmail.com')
,('0916354822','Arnold','Mendoza','operador2@hotmail.com')
,('0816475635','Micaela','Cabezas','operador3@hotmail.com')
,('0917563545','Sofia','Rojas','operador4@hotmail.com')
,('0987767616','Ana','Peraltes','operador5@hotmail.com');

#PACIENTE
INSERT INTO `paciente` (`Cedula`, `Nombre`, `Apellido`, `Direccion`, `Telefono`) VALUES
 ('0987654329', 'Johana', 'Cruz', 'Los Esteros mz 23 villa 12', '0932464325')
,('0932473268', 'Eduardo', 'Cruz', 'Floresta mz 2 villa 11', '0854476934')
,('0912378163', 'Emilia', 'Sanchez', 'Sauces 8 mz 3 villa 1', '0987654327')
,('0912371263', 'Juan', 'Fajardo', 'Urdenor 1 mz 2 villa 1', '0852316653')
,('0927412822', 'Josue', 'Davalos', 'La Joya Etapa Safiro', '0921372671');

#PATODOLOGIAS
INSERT INTO `centro_dermatologico`.`patologias` (`Nombre`, `Descripcion`) VALUES
 ('Dermatitis', 'hipersensibilidad en piel')
,('Eczema', 'Piel inflamada')
,('Psoriasis', 'Erupciones')
,('Alopecia', 'caída de cabello')
,('Urticaria', 'Ronchas rojizas')
,('Acné', 'Folículos Inflamados');

#CITA
INSERT INTO `centro_dermatologico`.`cita` (`Id_Paciente`, `Id_Doctor`, `Fecha`, `Hora`, `Color`, `activo`) VALUES
 ('0987654329', '0929538361', '2019-01-30', '12:00', 'rojo', true)
,('0932473268', '0929538361', '2019-01-30', '14:00', 'amarillo', true)
,('0912378163', '0924573812', '2019-01-29', '10:00', 'verde', true)
,('0912378163', '0932423423', '2019-01-10', '11:00', 'verde', false)
,('0927412822', '0932423423', '2019-01-29', '12:00', 'rojo', true)
,('0987654329', '0912312353', '2019-01-29', '13:00', 'verde', true);


#HISTORIAL_MEDICO
INSERT INTO `historial_medico` (`Id_Paciente`, `Id_Patologia`, `observaciones`) VALUES 
('0987654329', '1', 'Presenta cuadro normal de hipersensibilidad.')
,('0932473268', '2', 'Se notan ciertas porciones de la piel laceradas.')
,('0912378163', '3', 'Nada fuera de lo normal.')
,('0912371263', '4', 'Alopecia prematura, presenta cuadro desde los 15 años.')
,('0927412822', '5', 'Urticaria aguda presenta cuadro desde hace menos de 6 semanas.')
,('0912378163', '6', 'Presenta costras con erupciones en la piel, además de espinillas, pústulas y quistes. Cuadro grave.')
,('0927412822', '1', 'Cuadro normal, características comúnes y fáciles de tratar.');

#RECETA
INSERT INTO `centro_dermatologico`.`receta` (`Id_Paciente`, `Id_Doctor`, `Id_Medicamento`, `Frecuencia`, `Dosis`) VALUES ('0912371263', '0929538361', '1', '2 c/dia', '2');
INSERT INTO `centro_dermatologico`.`receta` (`Id_Paciente`, `Id_Doctor`, `Id_Medicamento`, `Frecuencia`, `Dosis`) VALUES ('0912378163', '0929538361', '2', '2 c/dia', '2');
INSERT INTO `centro_dermatologico`.`receta` (`Id_Paciente`, `Id_Doctor`, `Id_Medicamento`, `Frecuencia`, `Dosis`) VALUES ('0927412822', '0932423423', '3', '2 c/dia', '2');
INSERT INTO `centro_dermatologico`.`receta` (`Id_Paciente`, `Id_Doctor`, `Id_Medicamento`, `Frecuencia`, `Dosis`) VALUES ('0927412822', '0912312353', '4', '2 c/dia', '2');
INSERT INTO `centro_dermatologico`.`receta` (`Id_Paciente`, `Id_Doctor`, `Id_Medicamento`, `Frecuencia`, `Dosis`) VALUES ('0927412822', '0924573812', '5', '2 c/dia', '2');
