/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Operador;

import funcional.Util;
import java.io.FileNotFoundException;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author joanp
 */
public final class TurnosPane {
    private final BorderPane root;
    
     public TurnosPane() throws FileNotFoundException{
        root = new BorderPane();
        
        root.setTop(Util.top());
        root.setBottom(TurnosButtom());
        root.setCenter(TurnosCenter());
     }
    
    public Pane TurnosCenter(){
        VBox result = new VBox();

        Text text = Util.styleText("VERIFICACION DE TURNOS", "Verdana", 35,true);
        //text.setStyle("-fx-font-weight: bold");
        
        HBox hb = new HBox();
        Text date = Util.styleText("Fecha","Verdana",25,false); 
        Text esp = Util.styleText("Especialidad","Verdana",25,false);
        Button ver = new Button("Verificar");
        ver.setFont(Font.font("Verdana",25));
        GridPane gridPane = new GridPane();  
        gridPane.setPadding(new Insets(10, 10, 10, 10)); 
        gridPane.setVgap(40); 
        gridPane.setHgap(70);
        gridPane.add(date, 0, 0); 
        gridPane.add(new TextField(), 1, 0); 
        gridPane.add(esp, 0, 1);       
        gridPane.add(new TextField(), 1, 1);
        
        hb.getChildren().addAll(gridPane,ver);
        hb.setSpacing(100); hb.setAlignment(Pos.CENTER);
        
        result.getChildren().addAll(text,hb);
        result.setAlignment(Pos.CENTER);
        result.setSpacing(40);
        result.setStyle("-fx-background-color: #ffffff");
        return result;
    }
    
    public TableView TurnosButtom(){
        TableView table = new TableView();
        table.setEditable(true);
        TableColumn id = new TableColumn("IdDoctor"); id.setPrefWidth(300);
        TableColumn nom = new TableColumn("Nombres"); nom.setPrefWidth(300);
        TableColumn ap = new TableColumn("Apellidos"); ap.setPrefWidth(300);
        TableColumn espe = new TableColumn("Especialidad"); espe.setPrefWidth(300);
        TableColumn hor = new TableColumn("Horario"); hor.setPrefWidth(300);
        TableColumn fecha = new TableColumn("Fecha"); fecha.setPrefWidth(300);
        TableColumn est = new TableColumn("Estado"); est.setPrefWidth(300);
        table.getColumns().addAll(id, nom, ap,espe,hor,fecha,est);
        table.setPrefHeight(300);
        
        return table;
    }

    public BorderPane getRoot() {
        return root;
    }
    
    
}
