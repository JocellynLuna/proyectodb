/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.time.LocalTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

/**
 *
 * @author Jocellyn Luna
 */
public class Validaciones {

    /**
     * Valida si el celular registrado si cumple las condiciones de un número
     * telefónico
     *
     * @param celular TextField número de celular del usuario
     * @return boolean indicando si es un número de celular válido
     */
    public static boolean validarCelular(TextField celular) {
        Pattern pattern;
        pattern = Pattern
                .compile("^(\\+593|0|593)?[ -]*[0-9][ -]*([0-9][ -]*){8}$");

        Matcher mather = pattern.matcher(celular.getText());

        if (!mather.find()) {
            celular.requestFocus();
            Metodos.mensajealerta("Número de celular no válido.");
            return false;

        }
        return true;
    }

    public static boolean validarHora(TextField hora) {
        Pattern pattern;
        pattern = Pattern
                .compile("^([01][0-9]|2[0-3]):[0-5][0-9]$");

        Matcher mather = pattern.matcher(hora.getText());

        if (!mather.find()) {
            hora.requestFocus();
            Metodos.mensajealerta("Formato de hora no válido.");
            return false;

        }
        return true;
    }

    /**
     * Valida si el correo registrado es en sí un correo
     *
     * @param correo TextField correo del usuario
     * @return boolean indicando si es un correo válido
     */
    public static boolean validarCorreo(TextField correo) {
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

        Matcher mather = pattern.matcher(correo.getText());

        if (!mather.find()) {
            correo.requestFocus();
            Metodos.mensajealerta("Correo no válido.");
            return false;

        }
        return true;
    }

    /**
     * Valida si el número ingresado es válido
     *
     * @param numero TextField número
     * @return boolean indicando si el texto no está vacío
     */
    public static boolean validarDouble(TextField numero) {
        try {
            Double.valueOf(numero.getText());
        } catch (NumberFormatException n) {
            numero.requestFocus();
            Metodos.mensajealerta("Debe recibir un numero.");
            return false;
        }
        return true;
    }

    /**
     * Valida si el número ingresado es válido como número entero
     *
     * @param numero TextField número
     * @return boolean indicanto si es aceptado como un número entero
     */
    public static boolean validarInt(TextField numero) {
        try {
            Integer.valueOf(numero.getText());
        } catch (NumberFormatException n) {
            numero.requestFocus();
            Metodos.mensajealerta("Debe recibir un numero.");
            return false;
        }
        return true;
    }

    /**
     * Valida si el número de indentificación registrado si cumple las
     * condiciones de la cédula
     *
     * @param numId TextField número de indentificación del usuario
     * @return boolean indicando si es un número de indentificación válido
     */
    public static boolean validarNumId(TextField numId) {
        Pattern pattern;
        pattern = Pattern.compile("^[0-9]{10}$");

        Matcher mather = pattern.matcher(numId.getText());

        if (!mather.find()) {
            numId.requestFocus();
            Metodos.mensajealerta("Número de cédula no válido.");
            return false;
        } else {
            return true;
        }
    }

    public static boolean validarTexto(TextField texto) {
        String cadena = texto.getText();
        cadena = cadena.replaceAll(" +", " ");

        if (cadena.equals("") || cadena.equals(" ")) {
            texto.requestFocus();
            Metodos.mensajealerta("Debe llenar todos los campos antes de continuar.");
            return false;
        }
        return true;
    }

    public static boolean validarTexto(TextArea texto) {
        String cadena = texto.getText();
        cadena = cadena.replaceAll(" +", " ");

        if (cadena.equals("") || cadena.equals(" ")) {
            texto.requestFocus();
            Metodos.mensajealerta("Debe llenar todos los campos antes de continuar.");
            return false;
        }
        return true;
    }

    public static boolean validarTextosPane(Pane p) {
        int i = 0;

        for (Node n : p.getChildren()) {
            if (n instanceof TextField) {
                TextField txt = (TextField) n;
                if (txt.getText().equals("") || txt.getText().equals(" ")) {
                    i += 1;
                }
            } else if (n instanceof TextArea) {
                TextArea txt = (TextArea) n;
                if (txt.getText().equals("") || txt.getText().equals(" ")) {
                    i += 1;
                }
            }
        }
        return i != 0;
    }
    
    public static boolean validarDosHoras(LocalTime l1, LocalTime l2){
        
        if(l1.isAfter(l2)){
            Metodos.mensajealerta("Su hora de ingreso no concuerda con la hora de salida.");
            return false;
        }else{
            return true;
        }
        
    }
}
