/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Usuarios.PDoctor;

import Modelo.Usuarios.PFarmaceutico.Medicamento;
import Modelo.Usuarios.POperador.Paciente;

/**
 *
 * @author Domenica Barreiro
 */
public class Receta {
    
    private Paciente paciente;
    private Doctor doctor;
    private Medicamento medicina;
    private String frecuencia;
    private String dosis;

    public Receta(Paciente paciente, Doctor doctor, Medicamento medicina, String frecuencia, String dosis) {
        this.paciente = paciente;
        this.doctor = doctor;
        this.medicina = medicina;
        this.frecuencia = frecuencia;
        this.dosis = dosis;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Medicamento getMedicina() {
        return medicina;
    }

    public void setMedicina(Medicamento medicina) {
        this.medicina = medicina;
    }

    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }

    public String getDosis() {
        return dosis;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }
    
    
    
}
