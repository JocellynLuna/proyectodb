/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Administrador;

import Modelo.Usuarios.Administrador;
import java.io.FileNotFoundException;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


/**
 *
 * @author Domenica Barreiro
 */
public class UIAdministrador {
    
    private final TabPane tabPane;
    private Administrador admin;
    private final Stage stage; 

    public UIAdministrador(Stage stage) throws FileNotFoundException {
        
        this.stage = stage;
        stage.setTitle("Administrador");
        Group root = new Group();
        Scene scene = new Scene(root,stage.getWidth(), stage.getHeight());
        
        //SE APLICA EL TABS------------------------------------------------------------
        tabPane = new TabPane();
        BorderPane borderPane = new BorderPane();
        //crea las secciones----------------------------------------------------
        crearTabs();
        // bind to take available space
        borderPane.prefHeightProperty().bind(scene.heightProperty());
        borderPane.prefWidthProperty().bind(scene.widthProperty());
        borderPane.setCenter(tabPane);
        root.getChildren().add(borderPane);
        stage.setScene(scene);
        stage.show();

    }
    
    /**
     * crea los tabs que se visualizaran en la pantalla y los redirecciona a otras clases
     * se crea el evento para actualizar el hilo de actualizacion
     */
    private void crearTabs() throws FileNotFoundException {
        
        Doctores doc = new Doctores(stage);
        Farmaceuticos far = new Farmaceuticos(stage);
        Operadores oper = new Operadores(stage);
        
        //TAB DOCTORES
        Tab t1 = doc.getDoctores();
        //TAB FARMACEUTICOS
        Tab t2 = far.getFarmaceuticos();
        //TAB OPERADORES
        Tab t3 = oper.getOperadores();
        
        tabPane.getTabs().addAll(t1,t2,t3);
    }

    //Listen
    
    /*tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            
        @Override
        public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
        Actualizable pantalla = null;
        switch( newValue.getText().replace(" ","")){
            case "Doctores": 
                pantalla=ord;
                break;
            case "Farmaceuticos": 
                pantalla = env;
                break;
            case "Operadores": 
                pantalla = cond;
                break;             
            }
            pantalla.Actualizar();
            hA.setPantalla(pantalla);
               
        }
        });*/

    public Stage getStage() {
        return stage;
    }

} 

