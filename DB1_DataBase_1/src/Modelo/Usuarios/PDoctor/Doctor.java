/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Usuarios.PDoctor;

import Modelo.Usuarios.POperador.Cita;
import Modelo.Usuarios.Usuario;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;

/**
 *
 * @author Domenica Barreiro
 */
public class Doctor extends Usuario {
    
    private String telefono;
    private LocalDate fecha_ingreso;
    private LocalTime hora_ingreso;
    private LocalTime hora_salida;
    private String especialidad;
    private String cargo;
    private LinkedList<Cita> citas;
   
    
    /**
     * Contructor del objeto Doctor
     * @param nombre String con nombre del doctor
     * @param apellido String con apellido del doctor
     * @param cedula String con cedula del doctor
     * @param correo String con correo del doctor
     * @param contrasenia String con contraseÃ±a del doctor
     * @param telefono String con el telefono del doctor
     * @param fecha_in LocalDate con la fecha de ingreso del doctor
     * @param hora_in LocalTime con la hora de ingreso del doctor
     * @param hora_sal LocalTime con la hora de salida del doctor
     * @param espe String con la especialidad del doctor
     * @param cargo String con el cargo del doctor
     */
    public Doctor (String nombre, String apellido, String cedula, String correo, 
            String contrasenia, String telefono, LocalDate fecha_in, LocalTime hora_in, 
            LocalTime hora_sal, String espe, String cargo) {
        
        super(nombre,apellido,cedula,correo,contrasenia);
        this.telefono = telefono;
        this.fecha_ingreso = fecha_in;
        this.hora_ingreso = hora_in;
        this.hora_salida = hora_sal;
        this.especialidad = espe;
        this.cargo = cargo;
        citas = new LinkedList<>();
    }
    
    
    /*funcion crear historial medico*/
    
    /*funcion crear receta*/

    
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public LocalDate getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(LocalDate fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public LocalTime getHora_ingreso() {
        return hora_ingreso;
    }

    public void setHora_ingreso(LocalTime hora_ingreso) {
        this.hora_ingreso = hora_ingreso;
    }

    public LocalTime getHora_salida() {
        return hora_salida;
    }

    public void setHora_salida(LocalTime hora_salida) {
        this.hora_salida = hora_salida;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    
    public boolean agregarCita(Cita c){
        if(citas.contains(c))
            return false;
        citas.add(c);
        return true;
    }
}
