/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Operador;

import funcional.Util;
import java.io.FileNotFoundException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author joanp
 */
public final class ConsultarCita {
    private final BorderPane root;
    private final Stage origen;
    
    public ConsultarCita(Stage stage) throws FileNotFoundException{
        root = new BorderPane();
        this.origen = stage;
        
        root.setTop(Util.operadorTop());
        root.setCenter(Util.operadorCenter("Consultar Cita", "Puede realizar la búsqueda ingresando el ID (código) de la cita", 50));
        root.setBottom(citaBottom());
        
        root.setStyle("-fx-background-image: url('"+getClass().getResource("/recursos/fondo_cita.jpg").toExternalForm()+"');"
                                        + "-fx-background-repeat: stretch;"  
                                       + "-fx-background-size: "+stage.getWidth()+" "+stage.getHeight()+";"                
                                        + "-fx-background-position: center;");
    }
    
    public AnchorPane citaBottom() throws FileNotFoundException{
        //HBox busqueda = new HBox(Util.styleText("ID Cita   ", "Verdana", 25, false),Util.sizeTextField(250, 40));
        //busqueda.setAlignment(Pos.CENTER);
        
        TableView tabla = Util.tablaCita();
        tabla.setPrefHeight(350);
        
        
        Button regresar = Util.botonImagen("  Regresar","src/recursos/regresar.png", 50, 50);
        regresar.setOnAction(new ManejadorRegresar());
        
        HBox botones = new HBox(regresar);
        botones.setSpacing(50);
        
        VBox result = new VBox(tabla,botones);
        result.setSpacing(40);
        botones.setAlignment(Pos.CENTER);
        
        AnchorPane ap = new AnchorPane(result);
        AnchorPane.setLeftAnchor(result, 50.0);
        AnchorPane.setTopAnchor(result, 25.0);
        AnchorPane.setBottomAnchor(result, 125.0);
        AnchorPane.setRightAnchor(result, 50.0);
        
        return ap;
    }

    public BorderPane getRoot() {
        return root;
    }
    
   private class ManejadorRegresar implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                SesionOperador sop = new SesionOperador(origen);
                origen.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                origen.setScene(sop.getScene());
                origen.centerOnScreen (); 
                origen.setMaximized(true);
                origen.show();
            } catch (FileNotFoundException ex) {
            }
        } 
    }
}
