/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Operador;

import funcional.Util;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author joanp
 */
public class CitaPane {
    private BorderPane root;

    public CitaPane() throws FileNotFoundException {
        root = new BorderPane();
        
        root.setTop(Util.top());
        root.setBottom(Util.button());
        root.setCenter(CitaCenter());
    }
    
    public Pane CitaCenter(){
        VBox result = new VBox();
        
        Text text = Util.styleText("INGRESO DE LA CITA", "Verdana", 35,true);
        //text.setStyle("-fx-font-weight: bold");
        
        Text id = Util.styleText("Cedula","Verdana",25,false);
        Text date = Util.styleText("Fecha","Verdana",25,false); 
        Text hor = Util.styleText("Hora","Verdana",25,false); 
        Text pa = Util.styleText("Patologia","Verdana",25,false);
        Text idD =Util.styleText("IdDoctor","Verdana",25,false); 
        Text esp = Util.styleText("Especialidad","Verdana",25,false);
        
        GridPane gridPane = new GridPane();  
        gridPane.setPadding(new Insets(10, 10, 10, 10)); 
        gridPane.setVgap(40); 
        gridPane.setHgap(70);
        gridPane.add(id, 0, 0); 
        gridPane.add(new TextField(), 1, 0); 
        gridPane.add(date, 0, 1);       
        gridPane.add(new TextField(), 1, 1); 
        gridPane.add(hor, 0, 2); 
        gridPane.add(new TextField(), 1, 2);
        gridPane.add(pa, 2, 0); 
        gridPane.add(new TextField(), 3, 0);
        gridPane.add(idD, 2, 1); 
        gridPane.add(new TextField(), 3, 1);
        gridPane.add(esp, 2, 2); 
        gridPane.add(new TextField(), 3, 2);
        gridPane.setAlignment(Pos.CENTER);
        
        result.getChildren().addAll(text,gridPane);
        result.setAlignment(Pos.CENTER);
        result.setSpacing(40);
        result.setStyle("-fx-background-color: #ffffff");
        return result;
    }
    

    public BorderPane getRoot() {
        return root;
    }
    
    
}
