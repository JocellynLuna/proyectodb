/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Operador;

import funcional.Util;
import java.io.FileNotFoundException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author joanp
 */
public final class VerificarDisponibilidad {
    private final BorderPane root;
    private Stage origen;
    
    public VerificarDisponibilidad(Stage stage) throws FileNotFoundException{
        root = new BorderPane();
        this.origen = stage;
        
        root.setTop(Util.operadorTop());
        root.setCenter(Util.styleText("Verificar Disponibilidad", "Verdada", 60, true));
        
        root.setBottom(disponibleBottom());
        
        root.setStyle("-fx-background-image: url('"+getClass().getResource("/recursos/fondo_disponibilidad.jpg").toExternalForm()+"');"
                                        + "-fx-background-repeat: stretch;"  
                                       + "-fx-background-size: "+stage.getWidth()+" "+stage.getHeight()+";"                
                                        + "-fx-background-position: center;");
    }
    
    public TableView tabla(){
        TableView table = new TableView();
        table.setEditable(true);
        TableColumn id = new TableColumn("ID Doctor"); id.setPrefWidth(300);
        TableColumn nom = new TableColumn("Nombre"); nom.setPrefWidth(300);
        TableColumn ape = new TableColumn("Apellido"); ape.setPrefWidth(300);
        TableColumn esp = new TableColumn("Especialidad"); esp.setPrefWidth(300);
        TableColumn tel = new TableColumn("Telefono"); tel.setPrefWidth(300);
        TableColumn hin = new TableColumn("Hora Ingreso"); hin.setPrefWidth(300);
        TableColumn hsal = new TableColumn("Hora Salida"); hsal.setPrefWidth(300);
        table.getColumns().addAll(id, nom, ape,esp,hin,hsal);
        table.setPrefHeight(300);
        
        return table;
    }
    
    public AnchorPane disponibleBottom() throws FileNotFoundException{
        TableView tabla = tabla();
        tabla.setPrefHeight(400);
        
        Button regresar = Util.botonImagen("  Regresar","src/recursos/regresar.png", 50, 50);
        regresar.setOnAction(new ManejadorRegresar());
        HBox botones = new HBox(regresar);
        botones.setAlignment(Pos.CENTER);
        
        VBox result = new VBox(tabla,botones);
        result.setSpacing(30);
        
        AnchorPane ap = new AnchorPane(result);
        AnchorPane.setLeftAnchor(result, 50.0);
        AnchorPane.setTopAnchor(result, 20.0);
        AnchorPane.setBottomAnchor(result, 40.0);
        AnchorPane.setRightAnchor(result, 50.0);
        
        return ap;
    }

    public BorderPane getRoot() {
        return root;
    }
    
    private class ManejadorRegresar implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                SesionOperador sop = new SesionOperador(origen);
                origen.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                origen.setScene(sop.getScene());
                origen.centerOnScreen (); 
                origen.setMaximized(true);
                origen.show();
            } catch (FileNotFoundException ex) {
            }
        } 
    }
}
