/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.util.Optional;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

/**
 *
 * @author Jocelly Luna
 */
public class Metodos {
   public static Alert alert = new Alert(AlertType.INFORMATION);
   public  static double alto = 0.0;
   public  static double ancho = 0.0;
   
   
    /**
     * Muestra un cuadro de dialogo en la pantalla
     * @param mensaje mensaje que se muestra en el cuadro de dialogo
     */     
    public static void mensajealerta(String mensaje){
        alert.setAlertType(AlertType.INFORMATION);
        alert.setTitle("Aviso");
        alert.setHeaderText(null);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }
    
    
    
    /**
     * Muestra un cuadro de dialogo en donde pregunta si o no
     * @param mensaje mensaje que se mostrara en el cuadro de dialogo
     * @return true/false segun lo que indique el usuario
     */
    public static boolean comfirm(String mensaje){
        alert.setAlertType(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setContentText(mensaje);

        Optional<ButtonType> result = alert.showAndWait();
        return (result.get() == ButtonType.OK);
    }
    
    public static void vaciarTextos(Pane p){
        
        for(Node n: p.getChildren()){
            if(n instanceof TextField){
                TextField txt = (TextField)n;
                txt.setText("");    
            }else if(n instanceof TextArea){
                TextArea txt = (TextArea)n;
                txt.setText("");
            }
        }    
    }
 
    public static void setDimensiones(double alto,double ancho){
        Metodos.alto=alto;
        Metodos.ancho=ancho; 
    }

}
