/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funcional;

import BaseDeDatosConexion.ConexionBD;
import Datos.Metodos;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author joanp
 */
public class Util {
    
    public static ImageView imagen(String dir, int ancho, int largo) throws FileNotFoundException{
        FileInputStream in = new FileInputStream(dir);
        Image image1 = new Image(in);
        ImageView img = new ImageView(image1);
        img.setFitWidth(ancho);
        img.setFitHeight(largo);
        
        return img;
    }
    
    public static Pane top() throws FileNotFoundException {
        VBox result = new VBox();
        
        ImageView img = imagen("src/recursos/logo.PNG",500,250);
        
        Text us = Util.styleText("Usuario: ", "Verdana", 25,false);
        
        HBox hb = new HBox();
        Text text = Util.styleText("Sistema de Citas", "Verdana", 50,true);
        //text.setStyle("-fx-font-weight: bold");
        hb.getChildren().addAll(img,text);
        hb.setSpacing(50);
        hb.setAlignment(Pos.CENTER);
        
        result.getChildren().addAll(us,hb);
        result.setAlignment(Pos.CENTER_RIGHT);
        result.setStyle("-fx-background-color: #ffffff");
        result.setSpacing(50);
        return result;
    }
    
    public static Pane button(){
        HBox result = new HBox();
        
        Button ing = new Button("Ingresar");
        Button reg = new Button("Regresar");
        ing.setFont(Font.font("Verdana",25));
        reg.setFont(Font.font("Verdana",25));
        
        result.getChildren().addAll(ing,reg);
        result.setSpacing(100);
        result.setAlignment(Pos.TOP_CENTER);
        result.setStyle("-fx-background-color: #ffffff");
        result.setPrefHeight(200);
        return result;
    }
    
    public static Text styleText(String texto,String tipo, int tamanio,boolean negrita){
        Text text = new Text(texto);
        text.setFont(Font.font(tipo,tamanio));
        
        if(negrita)
            text.setStyle("-fx-font-weight: bold");
        
        return text;
    }
    
    public static Button botonImagen(String texto, String dir, int ancho,int largo) throws FileNotFoundException{
        ImageView iv = imagen(dir,ancho,largo);
        Button bt = new Button(texto,iv);
        bt.setFont(Font.font("Verdana",30));
        return bt;
    }
    
    public static TextField sizeTextField(int ancho,int largo){
        TextField tf = new TextField();
        tf.setPrefSize(ancho, largo);
        return tf;
    }
    
     public static TextArea sizeTextArea(int ancho,int largo){
        TextArea tf = new TextArea();
        tf.setPrefSize(ancho, largo);
        return tf;
    }
     
    public static HBox operadorTop() throws FileNotFoundException{
        ImageView logo = Util.imagen("src/recursos/logo.PNG", 500, 275);
        
        ImageView operador = Util.imagen("src/recursos/operador.png",250,250);
        Text detalle = Util.styleText("Bienvenido", "Verdana", 25,false);
        StackPane pane = new StackPane();
        pane.getChildren().addAll(operador,detalle);
        pane.setAlignment(Pos.BOTTOM_CENTER);
        
        HBox general = new HBox(logo,pane);
        general.setAlignment(Pos.BOTTOM_CENTER);
        general.setSpacing(800);
        general.setPrefHeight(325);
        
        return general;
    }
    
    public static HBox farmaceuticoTop() throws FileNotFoundException{
        ImageView logo = Util.imagen("src/recursos/logo.PNG", 500, 275);
        
        ImageView operador = Util.imagen("src/recursos/farmaceutico.png",250,250);
        Text detalle = Util.styleText("Bienvenido", "Verdana", 25,false);
        StackPane pane = new StackPane();
        pane.getChildren().addAll(operador,detalle);
        pane.setAlignment(Pos.BOTTOM_CENTER);
        
        HBox general = new HBox(logo,pane);
        general.setAlignment(Pos.BOTTOM_CENTER);
        general.setSpacing(800);
        general.setPrefHeight(325);
        
        return general;
    }
    
    public static AnchorPane operadorCenter(String titulo, String detalle, double sangria){
        AnchorPane ap = new AnchorPane();
        
        Text text = styleText(titulo, "Verdana", 60,true);
        Text detail = styleText(detalle, "Verdana", 30, true);
        
        VBox result = new VBox(text,detail);
        result.setAlignment(Pos.CENTER_LEFT);
        result.setSpacing(30);
        
        ap.getChildren().add(result);
        AnchorPane.setLeftAnchor(result, sangria);
        //AnchorPane.setTopAnchor(result, 0.0);
        //AnchorPane.setBottomAnchor(result, 0.0);
        return ap;
    }
    
    public static TableView tablaCita(){
        TableView table = new TableView();
        table.setEditable(true);
        TableColumn id = new TableColumn("ID Cita"); id.setPrefWidth(300);
        id.setCellValueFactory(new PropertyValueFactory<>("cedula"));
        
        TableColumn pac = new TableColumn("Paciente"); pac.setPrefWidth(300);
        pac.setCellValueFactory(new PropertyValueFactory<>("paciente"));
        
        TableColumn doc = new TableColumn("Doctor"); doc.setPrefWidth(300);
        doc.setCellValueFactory(new PropertyValueFactory<>("doctor"));
        
        TableColumn fecha = new TableColumn("Fecha"); fecha.setPrefWidth(300);
        fecha.setCellValueFactory(new PropertyValueFactory<>("fecha"));
        
        TableColumn hora = new TableColumn("Hora"); hora.setPrefWidth(300);
        hora.setCellValueFactory(new PropertyValueFactory<>("hora"));
        
        TableColumn color = new TableColumn("Color"); color.setPrefWidth(300);
        color.setCellValueFactory(new PropertyValueFactory<>("color"));
        
        table.getColumns().addAll(id, pac, doc,fecha,hora,color);
        table.setPrefHeight(300);
        cargarCitas(table);
        
        return table;
    }
    
    public static void cargarCitas(TableView table){
        table.getItems().clear();
        try {
            //DEBE IR LA CONEXIÓN CON LA BASE PARA PEDIR TODOS LOS DOCTORES DE LA TABLA
        table.getItems().addAll(ConexionBD.obtenerCitas());
        } catch (SQLException ex) {
            Metodos.mensajealerta(ex.getMessage());
        } 
    }
    
    public static TableView tablaPaciente(){
        TableView table = new TableView();
        table.setEditable(true);
        TableColumn id = new TableColumn("Cedula"); id.setPrefWidth(300);
        id.setCellValueFactory(new PropertyValueFactory<>("cedula"));
        
        TableColumn nom = new TableColumn("Nombre"); nom.setPrefWidth(300);
        nom.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        
        TableColumn ap = new TableColumn("Apellido"); ap.setPrefWidth(300);
        ap.setCellValueFactory(new PropertyValueFactory<>("apellido"));
        
        TableColumn dir = new TableColumn("Dirección"); dir.setPrefWidth(300);
        dir.setCellValueFactory(new PropertyValueFactory<>("direccion"));
        
        TableColumn telf = new TableColumn("Teléfono"); telf.setPrefWidth(300);
        telf.setCellValueFactory(new PropertyValueFactory<>("telefono"));
        table.getColumns().addAll(id, nom, ap,dir,telf);
        table.setPrefHeight(350);
        cargarPacientes(table);
        return table;
    }
    
    public static void cargarPacientes(TableView table){
        table.getItems().clear();
        try {
            //DEBE IR LA CONEXIÓN CON LA BASE PARA PEDIR TODOS LOS DOCTORES DE LA TABLA
        table.getItems().addAll(ConexionBD.obtenerPacientes());
        } catch (SQLException ex) {
            Metodos.mensajealerta(ex.getMessage());
        } 
    }
    
}
