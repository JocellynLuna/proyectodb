/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Administrador;

import BaseDeDatosConexion.ConexionBD;
import Datos.Metodos;
import Modelo.Usuarios.PDoctor.Doctor;
import funcional.Util;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import principal.LoginPane;

/**
 *
 * @author Domenica Barreiro
 */
public final class Doctores {
    
    private TableView<Doctor> tvDoctores;
    private final double ancho;
    private final double alto;
    private final Tab Doctores;
    private final Stage stage;

    public Doctores(Stage stage) throws FileNotFoundException {
        stage.setTitle("Administrador");
        this.stage = stage;
        ancho = stage.getWidth();
        alto = stage.getHeight();
        
        Doctores = new Tab("Doctores                 ");
        VBox vb = new VBox(arriba(),grilla(),abajo());
        cargarGrilla();
        vb.setSpacing(20);
        vb.setStyle("-fx-background-image: url('"+getClass().getResource("/recursos/fondo_doctores.jpg").toExternalForm()+"');"
                                        + "-fx-background-repeat: stretch;"  
                                       + "-fx-background-size: "+ancho+" "+alto+";"                
                                        + "-fx-background-position: center;");
        
        Doctores.setContent(vb);
        Doctores.setClosable(false);
    }

    public Tab getDoctores() {
        return Doctores;
    }
    
    private VBox arriba() throws FileNotFoundException {
        
        ImageView im = Util.imagen("src/recursos/logo.png", 200, 100);
        VBox logo = new VBox(im);
        
        Button btnNuevo = new Button("Nuevo Doctor");
        btnNuevo.setOnAction(e -> {
            NuevoDoctor pn = new NuevoDoctor(stage, this);
            Stage main = pn.getNuevoDoctor();
            main.show();
        });
        
        VBox vb = new VBox(logo,btnNuevo);
        vb.setSpacing(10);
        vb.setPadding(new Insets(0,0,20,0));
        vb.setTranslateX(20);
        vb.setTranslateY(20);
        return vb;
    }
    
    private TableView<Doctor> grilla(){
        tvDoctores = new TableView<>();
        
        TableColumn colId = new TableColumn("Cédula");
        colId.setCellValueFactory(new PropertyValueFactory<>("cedula"));
        
        TableColumn colNombre = new TableColumn("Nombre");
        colNombre.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
        
        TableColumn colApellido = new TableColumn("Apellido");
        colApellido.setCellValueFactory(new PropertyValueFactory<>("Apellido"));
        
        TableColumn colTelef = new TableColumn("Teléfono");
        colTelef.setCellValueFactory(new PropertyValueFactory<>("telefono"));
        
        TableColumn colCorreo = new TableColumn("Correo");
        colCorreo.setCellValueFactory(new PropertyValueFactory<>("Correo"));
        
        TableColumn colFechaIngreso = new TableColumn("Fecha Ingreso");
        colFechaIngreso.setCellValueFactory(new PropertyValueFactory<>("fecha_ingreso"));
        
        TableColumn colHoraIngreso = new TableColumn("Hora Ingreso");
        colHoraIngreso.setCellValueFactory(new PropertyValueFactory<>("hora_ingreso"));
        
        TableColumn colHoraSalida = new TableColumn("Hora Salida");
        colHoraSalida.setCellValueFactory(new PropertyValueFactory<>("hora_salida"));
        
        TableColumn colEspecialidad = new TableColumn("Especialidad");
        colEspecialidad.setCellValueFactory(new PropertyValueFactory<>("Especialidad"));
        
        TableColumn colCargo = new TableColumn("Cargo");
        colCargo.setCellValueFactory(new PropertyValueFactory<>("Cargo"));
        
        colId.setMinWidth(90);
        colNombre.setMinWidth(150);
        colApellido.setMinWidth(150);
        colTelef.setMinWidth(90);
        colCorreo.setMinWidth(190);
        colFechaIngreso.setMinWidth(90);
        colHoraIngreso.setMinWidth(90);
        colHoraSalida.setMinWidth(90);
        colEspecialidad.setMinWidth(200);
        colCargo.setMinWidth(200);
        
        
        tvDoctores.getColumns().addAll(colId,colNombre,colApellido,colEspecialidad,
                colCargo,colFechaIngreso,colHoraIngreso,colHoraSalida,colTelef,colCorreo);
        tvDoctores.setMinWidth(ancho-50);
        tvDoctores.setMaxWidth(ancho-50);
        tvDoctores.setMinHeight(alto/3);
        tvDoctores.setTranslateX(20);
        
        return tvDoctores;
    }
    
    private VBox abajo() throws FileNotFoundException{
        
        Button btnCerrar = new Button("Cerrar Cesión",Util.imagen("src/recursos/CerrarSesion.png", 50, 50));
        
        btnCerrar.setOnAction(e -> {
            try {
                LoginPane lp = new LoginPane(stage);
                Scene scene = new Scene(lp.getRoot(), stage.getWidth(), stage.getHeight(), Color.WHITE);
                this.stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                this.stage.setScene(scene);
                this.stage.centerOnScreen ();
                ConexionBD.reiniciarUsuario();
                this.stage.setMaximized(true);
                this.stage.show();
            } catch (FileNotFoundException ex) {
            }
        });
        
        VBox vb = new VBox(btnCerrar);
        
        vb.setTranslateX(10);
        
        return vb;
    }
    
    protected void cargarGrilla(){
        
        tvDoctores.getItems().clear();
        try {
            //DEBE IR LA CONEXIÓN CON LA BASE PARA PEDIR TODOS LOS DOCTORES DE LA TABLA
            tvDoctores.getItems().addAll(ConexionBD.obtenerDoctores());
        } catch (SQLException ex) {
            Metodos.mensajealerta(ex.getMessage());
        } 
    }  
}
