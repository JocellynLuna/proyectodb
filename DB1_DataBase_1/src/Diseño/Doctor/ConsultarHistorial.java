/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Doctor;

import BaseDeDatosConexion.ConexionBD;
import Datos.Metodos;
import Datos.Validaciones;
import Modelo.Usuarios.PDoctor.Doctor;
import Modelo.Usuarios.PDoctor.Historial_Medico;
import funcional.Util;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author Gisella Palacios
 */
public class ConsultarHistorial {
    
    private TableView<Historial_Medico> tvHistorial;
    private double ancho;
    private double alto;
    private TextField ingreso;
    private VBox root;
    private Scene scene;
    private final Stage main;
    
    public ConsultarHistorial(Stage stage, Scene sc) throws FileNotFoundException{
        main = stage;
        scene = sc;
        ancho = stage.getWidth();
        alto = stage.getHeight();
        
        Text titulo = Util.styleText("Consulte el Historial Médico de un Paciente","Verdana",25,true);
        
        root = new VBox(titulo,crearArriba(),grilla());
        root.setSpacing(20);
        root.setPadding(new Insets(20,0,0,0));
    }

    public VBox getRoot() {
        return root;
    }
    
    
    private HBox crearArriba() throws FileNotFoundException{
        
        
        Text bus = Util.styleText("No. de Cédula","Verdana",14,false);
        ingreso = new TextField();
        ImageView im = Util.imagen("src/recursos/buscar.png",20,20);
        Button buscar = new Button("Buscar");
        buscar.setOnAction(new ManejadorBuscar());
        
        ImageView im1 = Util.imagen("src/recursos/regresar.png",50,50);
        Button regresar = new Button("Regresar",im1);
        regresar.setOnAction(new ManejadorRegresar()); 
        
        HBox contenedor = new HBox(bus,ingreso,buscar);
        contenedor.setSpacing(20);
        contenedor.setAlignment(Pos.CENTER_LEFT);
        contenedor.setPadding(new Insets(20,20,20,20));
        
        HBox contenedor1 = new HBox(contenedor,regresar);
        contenedor1.setSpacing(1000);
        return contenedor1;
    }
    
    private TableView<Historial_Medico> grilla() {
        tvHistorial = new TableView<>();

        TableColumn colId = new TableColumn("Cédula del Paciente");
        colId.setCellValueFactory(new PropertyValueFactory<>("cedulaP"));

        TableColumn colNombre = new TableColumn("Nombre del Paciente");
        colNombre.setCellValueFactory(new PropertyValueFactory<>("nombreP"));
        
        TableColumn colApellido = new TableColumn("Apellido del Paciente");
        colApellido.setCellValueFactory(new PropertyValueFactory<>("apellidoP"));
        
        TableColumn nomPat = new TableColumn("Nombre de la Patología");
        nomPat.setCellValueFactory(new PropertyValueFactory<>("nomPat"));
        
        TableColumn desPat = new TableColumn("Descripción de la Patología");
        desPat.setCellValueFactory(new PropertyValueFactory<>("desPat"));
        
        TableColumn obs = new TableColumn("Observaciones");
        obs.setCellValueFactory(new PropertyValueFactory<>("observaciones"));
        
        colId.setMinWidth(200);
        colNombre.setMinWidth(200);
        colApellido.setMinWidth(200);
        nomPat.setMinWidth(200);
        desPat.setMinWidth(300);
        obs.setMinWidth(400);
        
        tvHistorial.getColumns().addAll(colId, colNombre, colApellido,nomPat,desPat,obs);
        tvHistorial.setMinWidth(ancho - 50);
        tvHistorial.setMaxWidth(ancho - 50);
        tvHistorial.setMinHeight(alto / 3);
        tvHistorial.setTranslateX(20);

        return tvHistorial;
    }
    
    private class ManejadorBuscar implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            tvHistorial.getItems().clear();
            if(Validaciones.validarNumId(ingreso)){
                try {
                    tvHistorial.getItems().addAll(ConexionBD.buscarHistorial(ingreso.getText()));
                } catch (SQLException ex) {
                    Metodos.mensajealerta(ex.getMessage());
                }
            }
        
        } 
    }
    
    private class ManejadorRegresar implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            main.setScene(scene);
            main.centerOnScreen();
            main.setMaximized(true);
            main.show();
        } 
    }
    
}
