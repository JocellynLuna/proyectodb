/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Farmaceutico;


import java.io.FileNotFoundException;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * @author Domenica Barreiro
 */
public class UIFarma {
    
    private final TabPane tabPane;
    private final Stage stage;
    
    public UIFarma(Stage stage) throws FileNotFoundException{
        this.stage = stage;
        stage.setTitle("Administrador");
        Group root = new Group();
        Scene scene = new Scene(root,stage.getWidth(), stage.getHeight());
        
        //SE APLICA EL TABS------------------------------------------------------------
        tabPane = new TabPane();
        BorderPane borderPane = new BorderPane();
        //crea las secciones----------------------------------------------------
        crearTabs();
        // bind to take available space
        borderPane.prefHeightProperty().bind(scene.heightProperty());
        borderPane.prefWidthProperty().bind(scene.widthProperty());
        borderPane.setCenter(tabPane);
        root.getChildren().add(borderPane);
        stage.setScene(scene);
        stage.show();
    }
    
    private void crearTabs() throws FileNotFoundException{
        
        ConsultarMedicamento cm = new ConsultarMedicamento(stage);
        NuevoMedicamento nm = new NuevoMedicamento(stage);
        
        //TAB DOCTORES
        Tab t1 = cm.getConsultar();
        //TAB FARMACEUTICOS
        Tab t2 = nm.getConsultar();
        
        tabPane.getTabs().addAll(t1,t2);
    }
    
}
