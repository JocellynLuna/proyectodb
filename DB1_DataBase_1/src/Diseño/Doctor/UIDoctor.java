/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Doctor;

import BaseDeDatosConexion.ConexionBD;
import Datos.Metodos;
import Modelo.Usuarios.PDoctor.Doctor;
import Modelo.Usuarios.POperador.Cita;
import funcional.Util;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import principal.LoginPane;

/**
 *
 * @author Domenica Barreiro
 */
public class UIDoctor {
    
    private Stage stage;
    private Doctor doc;
    private BorderPane root;
    private TableView<Cita> tvCitas;
    private Scene scene;
    private double ancho;
    private double alto;
    
    public UIDoctor(Stage stage, Doctor doctor) throws FileNotFoundException {

        this.stage = stage;
        this.doc = doctor;
        ancho = stage.getWidth();
        alto = stage.getHeight();
        
        stage.setTitle("Doctor");
        root = new BorderPane();
        
        scene = new Scene(root,ancho, alto);
        // bind to take available space
        root.prefHeightProperty().bind(scene.heightProperty());
        root.prefWidthProperty().bind(scene.widthProperty());
        root.setStyle("-fx-background-image: url('"+getClass().getResource("/recursos/ui_doctor.jpg").toExternalForm()+"');"
                                        + "-fx-background-repeat: stretch;"  
                                       + "-fx-background-size: "+ancho+" "+alto+";"                
                                        + "-fx-background-position: center;");
        
        root.setTop(crearTop());
        root.setCenter(crearCentro());
        cargarGrilla();
        
        stage.setScene(scene);
        stage.show();

    }
    
    private HBox crearTop() throws FileNotFoundException {
        
        ImageView logo = Util.imagen("src/recursos/logo.png", 200, 100);
        
        Label mensaje = new Label("Bienvenido/a, "+doc.getNombre()+" "+doc.getApellido()+".");
        mensaje.setStyle("-fx-font-size: 14pt");
        
        VBox contenedor = new VBox(logo,mensaje);
        contenedor.setAlignment(Pos.CENTER_LEFT);
        contenedor.setSpacing(10);
        contenedor.setPadding(new Insets(10,0,0,10));
        
        Button btnCerrar = new Button("Cerrar Cesión",Util.imagen("src/recursos/CerrarSesion.png", 50, 50));
        
        btnCerrar.setOnAction(e -> {
            try {
                LoginPane lp = new LoginPane(stage);
                Scene scene = new Scene(lp.getRoot(), ancho, alto, Color.WHITE);
                this.stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                this.stage.setScene(scene);
                this.stage.centerOnScreen ();
                ConexionBD.reiniciarUsuario();
                this.stage.setMaximized(true);
                this.stage.show();
            } catch (FileNotFoundException ex) {
            }
        });
        
        VBox boton = new VBox(btnCerrar);
        boton.setPadding(new Insets(40,0,0,20));
        
        HBox contenedor1 = new HBox(contenedor,boton);
        contenedor1.setSpacing(1100);
        
        return contenedor1;
    }
    
    
    private VBox crearCentro() throws FileNotFoundException {
        ImageView im = Util.imagen("src/recursos/nuevo.png", 20, 20);
        Button nuevoHistorial = new Button("Nuevo Historial Médico",im);
        nuevoHistorial.setOnAction(e -> {
            NuevoHistorial pn = new NuevoHistorial(stage);
            Stage main = pn.getNuevoHistorial();
            main.show();
        });
        
        ImageView im1 = Util.imagen("src/recursos/nuevo.png", 20, 20);
        Button nuevaReceta = new Button ("Nueva Receta Médica", im1);
        nuevaReceta.setOnAction(e -> {
            NuevaReceta pn = new NuevaReceta(stage);
            Stage main = pn.getNuevaReceta();
            main.show();
        });
        
        ImageView im2 = Util.imagen("src/recursos/buscar.png", 20, 20);
        Button buscarHistorial = new Button("Buscar Historial Médico",im2);
        buscarHistorial.setOnAction(e -> {
            
            try {
                ConsultarHistorial ch = new ConsultarHistorial(stage,scene);
                Scene scene1 = new Scene(ch.getRoot(), stage.getWidth(), stage.getHeight(), Color.WHITE);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(scene1);
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
                
            }
            
        });
        
        HBox botones  = new HBox(nuevoHistorial,nuevaReceta,buscarHistorial);
        botones.setAlignment(Pos.CENTER_LEFT);
        botones.setSpacing(30);
        
        Label titulo = new Label("Citas Próximas");
        titulo.setFont(Font.font("Verdana",23));
        
        VBox contenedor = new VBox(botones,titulo,grilla());
        contenedor.setAlignment(Pos.CENTER_LEFT);
        contenedor.setSpacing(10);
        contenedor.setPadding(new Insets(0,10,10,10));
        
        return contenedor;
    }
    
    
    private TableView<Cita> grilla(){
        
        tvCitas = new TableView<>();
        
        TableColumn colIdP = new TableColumn("Cédula del Paciente");
        colIdP.setCellValueFactory(new PropertyValueFactory<>("cedulaP"));
        
        TableColumn colNombreP = new TableColumn("Nombre del Paciente");
        colNombreP.setCellValueFactory(new PropertyValueFactory<>("nombreP"));
        
        TableColumn colApellidoP = new TableColumn("Apellido del Paciente");
        colApellidoP.setCellValueFactory(new PropertyValueFactory<>("apellidoP"));
        
        TableColumn colFecha = new TableColumn("Fecha");
        colFecha.setCellValueFactory(new PropertyValueFactory<>("fecha"));
        
        TableColumn colHora = new TableColumn("Hora");
        colHora.setCellValueFactory(new PropertyValueFactory<>("hora"));
        
        TableColumn colColor = new TableColumn("Color");
        colColor.setCellValueFactory(new PropertyValueFactory<>("colorN"));
        
        colIdP.setMinWidth(150);
        colNombreP.setMinWidth(150);
        colApellidoP.setMinWidth(150);
        colFecha.setMinWidth(90);
        colHora.setMinWidth(90);
        colColor.setMinWidth(50);
        
        tvCitas.getColumns().addAll(colIdP,colNombreP,colApellidoP,colFecha,
                colHora,colColor);
        tvCitas.setMinWidth(ancho-50);
        tvCitas.setMaxWidth(ancho-50);
        tvCitas.setMinHeight(alto/3);
        tvCitas.setTranslateX(20);
        
        return tvCitas;
    }
    
    
    private void cargarGrilla() {
        
        String cedula = this.doc.getCedula();
        
        //CÓDIGO PARA MOSTRAR LAS CITAS ASIGNADAS AL DOCTOR
        try {
            //DEBE IR LA CONEXIÓN CON LA BASE PARA PEDIR TODOS LOS DOCTORES DE LA TABLA
            tvCitas.getItems().addAll(ConexionBD.buscarCitaDeDoc(doc));
        } catch (SQLException ex) {
            Metodos.mensajealerta(ex.getMessage());
        } 
    }

    public Stage getStage() {
        return stage;
    }
}