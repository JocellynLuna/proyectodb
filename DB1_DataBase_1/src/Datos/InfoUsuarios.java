/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Excepciones.UsuarioExistente;
import Modelo.Usuarios.Administrador;
import Modelo.Usuarios.PDoctor.Doctor;
import Modelo.Usuarios.PFarmaceutico.Farmaceutico;
import Modelo.Usuarios.POperador.Operador;
import Modelo.Usuarios.Usuario;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.HashMap;

/**
 *
 * @author Domenica Barreiro
 */
public class InfoUsuarios {
    
    private static HashMap <String,Usuario> usuarios = new HashMap<>();
    private static final String ruta = "src/recursos/usuarios.dat";
    
     
     /**
      * Carga un mapa con los datos de los usuarios que se encuentran en el archivo
      * @return mapa con clave correo y value usuario
      */
    public static HashMap<String,Usuario> CargarUsuarios() {
        
        try(ObjectInputStream obj = new ObjectInputStream(new FileInputStream(ruta))){   
            
            return (HashMap<String,Usuario>)obj.readObject();
            
        }catch(ClassNotFoundException|IOException e){
            return new HashMap<>();
        }
   }
    
    
    /**
     * encuentra a un usuario con los datos pasados de parametro
     * @param correo correo del usuario
     * @param pass pass del usuario
     * @return 
     */
    public static Usuario login(String correo, String pass){
        recargar();
        if (usuarios.containsKey(correo)&&usuarios.get(correo).getContrasenia().equals(pass)){
            return usuarios.get(correo);
        }
        return null;
    } 
    
    
    /**
     * Guarda los datos cargados del sistema a los archivos
     */
    public static void guardarDat() {
        try(ObjectOutputStream obj = new ObjectOutputStream(new FileOutputStream(ruta,false))){
            obj.writeObject(usuarios);
        }catch(Exception e){
            System.out.println(e);
        }
    }
    
    /**
     * formatea el app deja solo la informacion del administrador y zonas
     */
    public static void inicializarDatos(){
        try{
            addUsuario(new Administrador("Admin","","","admin","admin"));
            addUsuario(new Doctor("Ginger", "Saltos", "0983552512", "doctor1", 
            "doctor1", "0946512325", LocalDate.of(2018, Month.MARCH, 21), LocalTime.of(8,30), 
            LocalTime.of(17,0), "Pediatría", "Cirujano de niños"));
            addUsuario(new Doctor("Lisette", "Cabello", "0983784512", "doctor2", 
            "doctor2", "0995874635", LocalDate.of(2018, Month.OCTOBER, 17), LocalTime.of(9,0), 
            LocalTime.of(17,0), "Belleza", "Médico Cutáneo"));
            addUsuario(new Farmaceutico("Farma1","","","farma1","farma1"));
            addUsuario(new Farmaceutico("Farma2","","","farma2","farma2"));
            addUsuario(new Operador("OP1","","","op1","op1"));
            addUsuario(new Operador("OP2","","","op2","op2"));
            guardarDat();
        }catch(Exception e){}
    }
    
     
    /**
    * añade un usuario al mapa 
    * @param user conductor o administrador a añadir
    * @throws UsuarioExistente 
    */
    public static void addUsuario(Usuario user) throws UsuarioExistente{
        
        if(usuarios.containsKey(user.getCorreo())){
            throw new UsuarioExistente("Este usuario ya existe.");
        }
        usuarios.put(user.getCorreo(),user);
        
         
    }
    
     /**
      * Vuelve a cargar los datos en el sistema
      */
    public static void recargar(){
        usuarios = CargarUsuarios();
    }
}
