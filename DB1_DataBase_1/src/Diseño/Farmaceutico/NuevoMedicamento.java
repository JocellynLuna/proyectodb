/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Farmaceutico;

import funcional.Util;
import java.io.FileNotFoundException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import principal.LoginPane;

/**
 *
 * @author joanp
 */
public final class NuevoMedicamento {
    private final BorderPane root;
    private final Tab consultar;
    private final Stage origen;
    
    
    public NuevoMedicamento(Stage stage) throws FileNotFoundException{
        this.origen = stage;
        root = new BorderPane();
        
        root.setTop(Util.farmaceuticoTop());
        root.setCenter(Util.operadorCenter("Nuevo Medicamento", "Ingrese los datos del medicamento a ser almacenado en stock", 250));
        root.setBottom(nuevoBottom());
        
        root.setStyle("-fx-background-image: url('"+getClass().getResource("/recursos/fondo_medicina.jpg").toExternalForm()+"');"
                                        + "-fx-background-repeat: stretch;"  
                                       + "-fx-background-size: "+stage.getWidth()+" "+stage.getHeight()+";"                
                                        + "-fx-background-position: center;");
        
        consultar = new Tab("Nuevo Medicamento                 ");        
        consultar.setContent(root);
        consultar.setClosable(false);
    }
    
    public Pane nuevoBottom() throws FileNotFoundException{
        GridPane gridPane = datos();  
        
        Button guardar = Util.botonImagen("  Guardar ","src/recursos/guardar.png", 50, 50);
        Button regresar = Util.botonImagen("  Regresar","src/recursos/regresar.png", 50, 50);
        regresar.setOnAction(new ManejadorRegresar());
        
        VBox botones = new VBox(new HBox(),guardar,regresar);
        botones.setSpacing(50);
        HBox result = new HBox(gridPane,botones);
        result.setAlignment(Pos.CENTER);
        result.setSpacing(350);
        result.setPrefHeight(350);
        
        AnchorPane ap = new AnchorPane(result);
        AnchorPane.setLeftAnchor(result, 250.0);
        AnchorPane.setTopAnchor(result, 0.0);
        AnchorPane.setBottomAnchor(result, 50.0);

        return ap;
    }
    
    public GridPane datos(){
        GridPane gridPane = new GridPane();  
        
        gridPane.setVgap(20); 
        gridPane.setHgap(100);
        gridPane.add(Util.styleText("ID Medicamento","Verdana",30,false), 0, 0); 
        gridPane.add(Util.sizeTextField(525,40), 1, 0); 
        gridPane.add(Util.styleText("Nombre","Verdana",30,false), 0, 1);       
        gridPane.add(Util.sizeTextField(525,40), 1, 1); 
        gridPane.add(Util.styleText("Presentación","Verdana",30,false), 0, 2); 
        gridPane.add(Util.sizeTextField(525,40), 1, 2);
        gridPane.add(Util.styleText("Marca","Verdana",30,false), 0, 3); 
        gridPane.add(Util.sizeTextField(525,40), 1, 3);
        gridPane.add(Util.styleText("Cantidad","Verdana",30,false), 0, 4); 
        gridPane.add(Util.sizeTextField(525,40), 1, 4);
        
        return gridPane;
    }

    public BorderPane getRoot() {
        return root;
    }

    public Tab getConsultar() {
        return consultar;
    }

    public Stage getOrigen() {
        return origen;
    }
    
    private class ManejadorRegresar implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                LoginPane so = new LoginPane(origen);
                Scene scene = new Scene(so.getRoot(), 1000, 500, Color.WHITE);
                origen.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                origen.setScene(scene);
                origen.centerOnScreen (); 
                origen.setMaximized(true);
                origen.show();
            } catch (FileNotFoundException ex) {
            }
        } 
    }
}
