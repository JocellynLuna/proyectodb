/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Farmaceutico;

import BaseDeDatosConexion.ConexionBD;
import Datos.Metodos;
import funcional.Util;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import principal.LoginPane;

/**
 *
 * @author joanp
 */
public final class ConsultarMedicamento {
    private final BorderPane root;
    private final Tab consultar;
    private final Stage origen;
    
    public ConsultarMedicamento(Stage stage) throws FileNotFoundException{
        this.origen = stage;
        root = new BorderPane();
        
        root.setTop(Util.farmaceuticoTop());
        root.setCenter(Util.operadorCenter("Consultar Medicamento", "Puede realizar la búsqueda de los medicamentos existentes en stock por ID o por nombre", 50));
        root.setBottom(consultarBottom());
        
        root.setStyle("-fx-background-image: url('"+getClass().getResource("/recursos/fondo_medicina.jpg").toExternalForm()+"');"
                                        + "-fx-background-repeat: stretch;"  
                                       + "-fx-background-size: "+stage.getWidth()+" "+stage.getHeight()+";"                
                                        + "-fx-background-position: center;");
        
        consultar = new Tab("Consultar Medicamento                 ");        
        consultar.setContent(root);
        consultar.setClosable(false);
    }
    
    public Pane consultarBottom() throws FileNotFoundException{
        HBox id = new HBox(Util.styleText("ID Medicamento    ", "Verdana", 25, false),Util.sizeTextField(250, 40));
        HBox medicamento = new HBox(Util.styleText("Medicamento    ", "Verdana", 25, false),Util.sizeTextField(250, 40));
        HBox top = new HBox(id,medicamento);
        top.setAlignment(Pos.CENTER);
        top.setSpacing(100);
        
        Button buscar = Util.botonImagen("  Buscar ","src/recursos/buscar.png", 50, 50);
        Button regresar = Util.botonImagen("  Regresar","src/recursos/regresar.png", 50, 50);
        regresar.setOnAction(new ManejadorRegresar());
        HBox botones = new HBox(buscar, regresar);
        botones.setSpacing(50);
        botones.setAlignment(Pos.CENTER);
        
         VBox result = new VBox(top,tabla(),botones);
        result.setSpacing(30);
        
        AnchorPane ap = new AnchorPane(result);
        AnchorPane.setLeftAnchor(result, 50.0);
        AnchorPane.setTopAnchor(result, 20.0);
        AnchorPane.setBottomAnchor(result, 20.0);
        AnchorPane.setRightAnchor(result, 50.0);
        
        return ap;
    }
    
    public TableView tabla(){
        TableView table = new TableView();
        table.setEditable(true);
        TableColumn id = new TableColumn("ID"); id.setPrefWidth(300);
        TableColumn med = new TableColumn("Medicamento"); med.setPrefWidth(300);
        TableColumn pre = new TableColumn("Presentación"); pre.setPrefWidth(300);
        TableColumn mar = new TableColumn("Marca"); mar.setPrefWidth(300);
        TableColumn can = new TableColumn("Cantidad"); can.setPrefWidth(300);
        
        table.getColumns().addAll(id, med, pre,mar,can);
        table.setPrefHeight(300);
        
        table.getItems().clear();
        try {
            //DEBE IR LA CONEXIÓN CON LA BASE PARA PEDIR TODOS LOS DOCTORES DE LA TABLA
            table.getItems().addAll(ConexionBD.obtenerMedicamentos());
        } catch (SQLException ex) {
            Metodos.mensajealerta(ex.getMessage());
        } 
        return table;
    }

    public BorderPane getRoot() {
        return root;
    }

    public Tab getConsultar() {
        return consultar;
    }

    public Stage getOrigen() {
        return origen;
    }
    private class ManejadorRegresar implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                LoginPane so = new LoginPane(origen);
                Scene scene = new Scene(so.getRoot(), 1000, 500, Color.WHITE);
                origen.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                origen.setScene(scene);
                origen.centerOnScreen (); 
                origen.setMaximized(true);
                origen.show();
            } catch (FileNotFoundException ex) {
            }
        } 
    }
    
}
