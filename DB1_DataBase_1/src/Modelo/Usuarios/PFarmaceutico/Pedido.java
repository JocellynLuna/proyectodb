/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Usuarios.PFarmaceutico;

import java.time.LocalDate;

/**
 *
 * @author Domenica Barreiro
 */
public class Pedido {
    
    private Proveedor proveedor;
    private Medicamento medicina;
    private LocalDate fecha;
    private int cantidad;

    public Pedido(Proveedor proveedor, Medicamento medicina, LocalDate fecha, int cantidad) {
        this.proveedor = proveedor;
        this.medicina = medicina;
        this.fecha = fecha;
        this.cantidad = cantidad;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public Medicamento getMedicina() {
        return medicina;
    }

    public void setMedicina(Medicamento medicina) {
        this.medicina = medicina;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    
    
}
