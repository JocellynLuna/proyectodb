/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Administrador;

import BaseDeDatosConexion.ConexionBD;
import Datos.InfoUsuarios;
import Datos.Metodos;
import Datos.Validaciones;
import Excepciones.UsuarioExistente;
import Modelo.Usuarios.PDoctor.Doctor;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Taws
 */
public class NuevoDoctor {
    
    private TextField nombre;
    private TextField apellido;
    private TextField cedula;
    private TextField correo;
    private TextField contrasenia;
    private TextField telefono;
    private DatePicker fecha_in;
    private TextField hora_in;
    private TextField hora_sal;
    private TextField espe;
    private TextField cargo;
    private LocalTime hour1;
    private LocalTime hour2;
    private VBox root;
    private final Stage main;
    private Doctores doctores;
    
    public NuevoDoctor (Stage principal, Doctores doc) {
        this.doctores = doc;
        main = new Stage();
        main.initModality(Modality.WINDOW_MODAL);
        main.initOwner(principal);
        
        main.setTitle("Nuevo Doctor");
        
        root = new VBox();
        
        root.setAlignment(Pos.CENTER);
        root.setPrefWidth(550);
        root.setPrefHeight(550);
        
        root.setPrefSize(550,550);
        root.setSpacing(15);
        root.setPadding(new Insets(10, 10, 20, 10));
        root.getChildren().addAll(CrearFormulario());
        root.setStyle("-fx-background-image: url('"+getClass().getResource("/recursos/FondoNuevoDoctor.jpg").toExternalForm()+"');"
                                        + "-fx-background-repeat: stretch;"  
                                       + "-fx-background-size: "+(root.getPrefWidth()+30)+" "+(root.getPrefHeight()+120)+";"                
                                        + "-fx-background-position: center;");
        Scene scene = new Scene(root);
        main.setScene(scene);
        
        
    }

    public Stage getNuevoDoctor() {
        return main;
    }
    
    
    private VBox CrearFormulario() {

        GridPane gpDatos = new GridPane();
        gpDatos.setAlignment(Pos.CENTER);
        gpDatos.setHgap(2);
        gpDatos.setVgap(15);

        //TITLES------------------------------------------------
        Label title = new Label("Agregar Doctor");
        HBox hb = new HBox(title);
        hb.setAlignment(Pos.CENTER_LEFT);
        title.setStyle("-fx-font-weight: bold;-fx-font-size: 20pt");

        Label da = new Label("Datos Personales: ");
        da.setStyle("-fx-font-weight: bold");
        gpDatos.add(da, 0, 0);

        //LABELS-------------------------------------------------
        gpDatos.add(new Label("Nombre"), 0, 1);
        GridPane.setHalignment(new Label("Nombre"), HPos.CENTER);
        gpDatos.add(new Label("Apellido"), 0, 2);
        gpDatos.add(new Label("Cédula"), 0, 3);
        gpDatos.add(new Label("Correo"), 0, 4);
        gpDatos.add(new Label("Contraseña"), 0, 5);
        gpDatos.add(new Label("Teléfono"), 0, 6);
        gpDatos.add(new Label("Fecha de Ingreso"), 0, 7);
        gpDatos.add(new Label("Hora de Ingreso"), 0, 8);
        gpDatos.add(new Label("Hora de Salida"), 0, 9);
        gpDatos.add(new Label("Especialidad"), 0, 10);
        gpDatos.add(new Label("Cargo"), 0, 11);

        //TEXTBOXS-----------------------------------------------
        nombre = new TextField();
        apellido = new TextField();
        cedula = new TextField();
        correo = new TextField();
        contrasenia = new TextField();
        telefono = new TextField();
        fecha_in = new DatePicker(LocalDate.now());
        fecha_in.setEditable(false);
        hora_in = new TextField();
        hora_sal = new TextField();
        espe = new TextField();
        cargo = new TextField();

        gpDatos.add(nombre, 1, 1);
        gpDatos.add(apellido, 1, 2);
        gpDatos.add(cedula, 1, 3);
        gpDatos.add(correo, 1, 4);
        gpDatos.add(contrasenia, 1, 5);
        gpDatos.add(telefono, 1, 6);
        gpDatos.add(fecha_in, 1, 7);
        gpDatos.add(hora_in, 1, 8);
        gpDatos.add(hora_sal, 1, 9);
        gpDatos.add(espe, 1, 10);
        gpDatos.add(cargo, 1, 11);

        //Botón-----------------
        Button btnGuardar = new Button("     Guardar     ");
                
        accionGuardar(btnGuardar, gpDatos);
        
        Button btncancelar = new Button("     Cancelar     ");
        btncancelar.setOnAction((e)->{
            main.close();
        });
        
        HBox hbG = new HBox(btnGuardar,btncancelar);
        hbG.setAlignment(Pos.CENTER);
        hbG.setSpacing(20);
        
        
        //Añadir ---------------------------------------------------------------------------------------------
        VBox vbDerecha = new VBox(hb, gpDatos, hbG);
        vbDerecha.setSpacing(20);
        vbDerecha.setAlignment(Pos.CENTER);
        
        return vbDerecha;

    }
    
    private void accionGuardar(Button btnGuardar, GridPane gpDatos){
        btnGuardar.setOnAction((e) -> {

            if (Validaciones.validarTextosPane(gpDatos)) {
                Metodos.mensajealerta("Debe llenar todos los campos antes de continuar.");
            } else if (fecha_in.getValue() == null) {
                Metodos.mensajealerta("Escoja una fecha de ingreso antes de continuar.");
            }else if (validarDatos()) {
                String name = nombre.getText();
                String surname = apellido.getText();
                String id = cedula.getText();
                String mail = correo.getText();
                String password = contrasenia.getText();
                String phone = telefono.getText();
                LocalDate date1 = fecha_in.getValue();
                hour1 = LocalTime.parse(hora_in.getText());
                hour2 = LocalTime.parse(hora_sal.getText());
                String specialty = espe.getText();
                String charge = cargo.getText();
                
                try {
                    ConexionBD.insertarDoctor(id, name, surname, phone,
                            mail, password, date1, hour1, hour2, specialty, charge);
                    
                    doctores.cargarGrilla();
                    
                    Metodos.mensajealerta("Se ha registrado la información correctamente!");
                    Metodos.vaciarTextos(gpDatos);
                    fecha_in.setValue(LocalDate.now());

                } catch (SQLException ex) {
                    Metodos.mensajealerta("Ocurrió un error. " + ex.getMessage());
                }
            }
        });
    }
    
    private boolean validarDatos() {
        return Validaciones.validarTexto(nombre)
                && Validaciones.validarTexto(apellido)
                && Validaciones.validarNumId(cedula)
                && Validaciones.validarCorreo(correo)
                && Validaciones.validarTexto(contrasenia)
                && Validaciones.validarCelular(telefono)
                && Validaciones.validarHora(hora_in)
                && Validaciones.validarHora(hora_sal)
                && Validaciones.validarTexto(espe)
                && Validaciones.validarTexto(cargo)
                && Validaciones.validarDosHoras(hour1,hour2);
    }
}
