/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Operador;

import Modelo.Usuarios.POperador.Cita;
import Modelo.Usuarios.POperador.Paciente;
import funcional.Util;
import java.io.FileNotFoundException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author joanp
 */
public final class ModificarPaciente {
    private final BorderPane root;
    private final Stage stage;
    private TableView table;
    public static Paciente pacienteModific;
    
    public ModificarPaciente(Stage stage) throws FileNotFoundException{
        root = new BorderPane();
        this.stage = stage;
        
        root.setTop(Util.operadorTop());
        root.setCenter(Util.operadorCenter("Modificar Paciente", "Puede modificar o eliminar los datos de los pacientes ingresados al sistema", 50));
        root.setBottom(pacienteBottom());
        
        root.setStyle("-fx-background-image: url('"+getClass().getResource("/recursos/fondo_paciente.jpg").toExternalForm()+"');"
                                        + "-fx-background-repeat: stretch;"  
                                       + "-fx-background-size: "+stage.getWidth()+" "+stage.getHeight()+";"                
                                        + "-fx-background-position: center;");
    }
    
    public TableView tabla(){
        table = new TableView();
        table.setEditable(true);
        TableColumn id = new TableColumn("Cedula"); id.setPrefWidth(300);
        id.setCellValueFactory(new PropertyValueFactory<>("cedula"));
        
        TableColumn nom = new TableColumn("Nombre"); nom.setPrefWidth(300);
        nom.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        
        TableColumn ap = new TableColumn("Apellido"); ap.setPrefWidth(300);
        ap.setCellValueFactory(new PropertyValueFactory<>("apellido"));
        
        TableColumn dir = new TableColumn("Dirección"); dir.setPrefWidth(300);
        dir.setCellValueFactory(new PropertyValueFactory<>("direccion"));
        
        TableColumn telf = new TableColumn("Teléfono"); telf.setPrefWidth(300);
        telf.setCellValueFactory(new PropertyValueFactory<>("telefono"));
        
        table.getColumns().addAll(id, nom, ap,dir,telf);
        table.setPrefHeight(300);
        Util.cargarPacientes(table);
        
        table.setOnMouseClicked((event) -> {
            if (event.getClickCount()==1) {
                onEdit();
            }
        });
        
        return table;
    }
    
    public Pane pacienteBottom() throws FileNotFoundException{
        AnchorPane ap = new AnchorPane();
        
        TableView tabla = tabla();
        Button guardar = Util.botonImagen("  Modificar ","src/recursos/guardar.png", 50, 50);
        guardar.setOnAction(new ManejadorGuardar());
        Button regresar = Util.botonImagen("  Regresar","src/recursos/regresar.png", 50, 50);
        regresar.setOnAction(new ManejadorRegresar());
        
        HBox botones = new HBox(guardar, regresar);
        botones.setSpacing(50);
        VBox result = new VBox(tabla,botones);
        result.setSpacing(40);
        botones.setAlignment(Pos.CENTER);
        
        ap.getChildren().add(result);
        AnchorPane.setLeftAnchor(result, 50.0);
        AnchorPane.setTopAnchor(result, 0.0);
        AnchorPane.setBottomAnchor(result, 100.0);
        AnchorPane.setRightAnchor(result, 50.0);
        
        return ap;
    }

    public BorderPane getRoot() {
        return root;
    }
    
    public Stage getOrigen() {
        return stage;
    }
    
    public void onEdit() {
        // check the table's selected item and get selected item
        if (table.getSelectionModel().getSelectedItem() != null) {
            pacienteModific = (Paciente) table.getSelectionModel().getSelectedItem();
            }


    }
    private class ManejadorGuardar implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                NuevoPaciente sop = new NuevoPaciente(stage);
                sop.setPaciente(pacienteModific);
                
                Scene scene = new Scene(sop.getRoot(),stage.getWidth(),stage.getHeight(),Color.WHITE);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(scene);
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
            }
        } 
    }
    
    private class ManejadorRegresar implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                SesionOperador sop = new SesionOperador(stage);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(sop.getScene());
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
            }
        } 
    }
}
