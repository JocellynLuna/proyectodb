/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Operador;

import Modelo.Usuarios.POperador.Cita;
import funcional.Util;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author joanp
 */
public final class ModificarCita {
    private  final BorderPane root;
    private final Stage stage;
    TableView table;
    public static Cita citaModific;
    
    public ModificarCita(Stage stage) throws FileNotFoundException{
        root = new BorderPane();
        this.stage = stage;
        
        root.setTop(Util.operadorTop());
        root.setCenter(Util.operadorCenter("Modificar Cita", "Puede modificar o eliminar las citas planificadas en  el sistema", 50));
        root.setBottom(citaBottom());
        
        root.setStyle("-fx-background-image: url('"+getClass().getResource("/recursos/fondo_cita.jpg").toExternalForm()+"');"
                                        + "-fx-background-repeat: stretch;"  
                                       + "-fx-background-size: "+stage.getWidth()+" "+stage.getHeight()+";"                
                                        + "-fx-background-position: center;");
    }
    
    public AnchorPane citaBottom() throws FileNotFoundException{
        TableView tabla = tabla();
        
        Button guardar = Util.botonImagen("  Modificar ","src/recursos/guardar.png", 50, 50);
        guardar.setOnAction(new ManejadorGuardar()); ///manejador
        Button regresar = Util.botonImagen("  Regresar","src/recursos/regresar.png", 50, 50);
        regresar.setOnAction(new ManejadorRegresar()); ///manejador
        HBox botones = new HBox(guardar, regresar);
        botones.setSpacing(50);
        
        VBox result = new VBox(tabla,botones);
        result.setSpacing(40);
        botones.setAlignment(Pos.CENTER);
        
        AnchorPane ap = new AnchorPane(result);
        AnchorPane.setLeftAnchor(result, 50.0);
        //AnchorPane.setTopAnchor(result, 5.0);
        AnchorPane.setBottomAnchor(result, 100.0);
        AnchorPane.setRightAnchor(result, 50.0);
        
        return ap;
    }
    
    public TableView tabla(){
        table = new TableView();
        table.setEditable(true);
        TableColumn id = new TableColumn("ID Cita"); id.setPrefWidth(250);
        id.setCellValueFactory(new PropertyValueFactory<>("cedula"));
        
        TableColumn pac = new TableColumn("Nombre Paciente"); pac.setPrefWidth(250);
        pac.setCellValueFactory(new PropertyValueFactory<>("nombreP"));
        
        TableColumn app = new TableColumn("Apellido Paciente"); app.setPrefWidth(250);
        app.setCellValueFactory(new PropertyValueFactory<>("apellidoP"));
        
        TableColumn doc = new TableColumn("Nombre Doctor"); doc.setPrefWidth(250);
        doc.setCellValueFactory(new PropertyValueFactory<>("nombreD"));
        
        TableColumn adoc = new TableColumn("Apellido Doctor"); adoc.setPrefWidth(250);
        adoc.setCellValueFactory(new PropertyValueFactory<>("apellidoD"));
        
        TableColumn fecha = new TableColumn("Fecha"); fecha.setPrefWidth(250);
        fecha.setCellValueFactory(new PropertyValueFactory<>("fecha"));
        
        TableColumn hora = new TableColumn("Hora"); hora.setPrefWidth(250);
        hora.setCellValueFactory(new PropertyValueFactory<>("hora"));
        
        TableColumn color = new TableColumn("Color"); color.setPrefWidth(300);
        color.setCellValueFactory(new PropertyValueFactory<>("color"));
        
        table.getColumns().addAll(id, pac,app, doc,adoc,fecha,hora,color);
        table.setPrefHeight(300);
        
        Util.cargarCitas(table);
        table.setOnMouseClicked((event) -> {
            if (event.getClickCount()==1) {
                onEdit();
            }
        });
        
        return table;
    }

    public BorderPane getRoot() {
        return root;
    }
    
    private class ManejadorRegresar implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                SesionOperador sop = new SesionOperador(stage);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(sop.getScene());
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
            }
        } 
    }
    
    public void onEdit() {
        // check the table's selected item and get selected item
        if (table.getSelectionModel().getSelectedItem() != null) {
            citaModific = (Cita) table.getSelectionModel().getSelectedItem();
            }


    }
    
    public static Cita getcita(){
        return citaModific;
    }
    
    private class ManejadorGuardar implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                NuevaCita sop = new NuevaCita(stage);
                System.out.println(sop);
                sop.setCita(citaModific);
                
                Scene scene = new Scene(sop.getRoot(),stage.getWidth(),stage.getHeight(),Color.WHITE);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(scene);
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
            }
        } 
    }
    
}
