/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Usuarios;

/**
 *
 * @author Domenica Barreiro
 */
public class Administrador extends Usuario{
    
    /**
     * Contructor del objeto Administrador 
     * @param nombre String con nombre del Usuario
     * @param apellido String con apellido del Usuario
     * @param cedula String con cedula del Usuario
     * @param correo String con correo del Usuario
     * @param contrasenia String con contraseÃ±a del Usuario
     */
    public Administrador (String nombre, String apellido, String cedula, String correo, String contrasenia) {
        super(nombre,apellido,cedula,correo,contrasenia);
    }
    
    /*public agregarDoctor();*/
    /*public agregarFarmaceutico();*/
    /*public agregarOperador();*/
    
}
