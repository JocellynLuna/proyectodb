/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import BaseDeDatosConexion.ConexionBD;
import Datos.Metodos;
import Diseño.Administrador.UIAdministrador;
import Diseño.Doctor.UIDoctor;
import Diseño.Farmaceutico.UIFarma;
import Diseño.Operador.SesionOperador;
import Modelo.Usuarios.Administrador;
import Modelo.Usuarios.PDoctor.Doctor;
import Modelo.Usuarios.PFarmaceutico.Farmaceutico;
import Modelo.Usuarios.POperador.Operador;
import Modelo.Usuarios.Usuario;
import funcional.Util;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author joanp y Domenica Barreiro
 */
public class LoginPane {
    
    private BorderPane root;
    private Stage stage;
    private TextField User;
    private PasswordField password;
    
    public LoginPane(Stage stage) throws FileNotFoundException {
        
        this.stage = stage;
        root = new BorderPane();
        root.setStyle("-fx-background-color: #ffffff");
        root.setTop(crearTop());
        root.setCenter(crearCentro());
        root.setBottom(crearButtom());
    }
    
    private VBox crearCentro(){
        Text titulo = Util.styleText("Login", "Verdana", 45,true);
        
        Text l1 = Util.styleText("Usuario", "Verdana", 30,false);
        Text l2 = Util.styleText("Contraseña", "Verdana", 30,false);
        
        User = Util.sizeTextField(300, 40);
        password = new PasswordField();
        password.setPrefSize(300, 40);
        password.autosize();
        
        VBox usuario = new VBox(l1,User);
        VBox contrasenia = new VBox(l2,password);

        HBox Us = new HBox(usuario);
        Us.setAlignment(Pos.CENTER);

        HBox con = new HBox(contrasenia);
        con.setAlignment(Pos.CENTER);
        
        VBox centro = new VBox(titulo,Us,con);
        centro.setSpacing(30);
        centro.setAlignment(Pos.CENTER);

    return centro;
    }
    
    private VBox crearTop() throws FileNotFoundException{
       
        ImageView img = Util.imagen("src/recursos/logo.PNG",500,250);
        VBox top = new VBox(img);
        top.setAlignment(Pos.BOTTOM_CENTER);
        top.setPadding(new Insets(20, 20, 30, 20));
        top.setSpacing(50);
        return top; 
    }
    
    private HBox crearButtom(){
        Button ingresar = new Button ("Ingresar");
        ingresar.setFont(Font.font("Verdana",25));
        accionIngresar(ingresar);
        
        HBox botones = new HBox(ingresar);
        botones.setAlignment(Pos.CENTER);
        botones.setPrefHeight(250);
        
        return botones;
    }
    
    private void accionIngresar(Button ingresar){
        ingresar.setOnAction(e -> {
            
            Usuario user = null;
            try {
                user = ConexionBD.buscarUsuario(User.getText(), password.getText());
            } catch (SQLException ex) {
                Metodos.mensajealerta(ex.getMessage());
            }
            
            if (user == null){
                Metodos.mensajealerta("Usuario o contraseña incorrecta");
            }
            else{

                Metodos.mensajealerta("Ingreso correcto");
                Metodos.setDimensiones(stage.getHeight(),stage.getWidth());
                if (user instanceof Administrador) {
                    try {
                        UIAdministrador admin = new UIAdministrador(stage);
                    } catch (FileNotFoundException ex) {
                        
                    }
                    
                }else if(user instanceof Doctor ){
                    try {
                        UIDoctor doc = new UIDoctor(stage,(Doctor)user);
                    } catch (FileNotFoundException ex) {
                    }
                    
                }else if(user instanceof Operador ){
                    try {
                        SesionOperador op = new SesionOperador(stage);
                    } catch (FileNotFoundException ex) {
                    }
                }
                
                else if(user instanceof Farmaceutico ){
                    try {
                        UIFarma farma = new UIFarma(stage);
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(LoginPane.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    } 
    
    public BorderPane getRoot() {
        return root;
    }
}
