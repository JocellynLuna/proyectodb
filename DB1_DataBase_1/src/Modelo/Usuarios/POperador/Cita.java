/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Usuarios.POperador;

import Modelo.Usuarios.PDoctor.Doctor;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 *
 * @author Domenica Barreiro
 */
public class Cita {
    private String idCita;
    private Paciente paciente;
    private String nombreP;
    private String cedulaP;
    private String apellidoP;
    private Doctor doctor;
    private String nombreD;
    private String cedulaD;
    private String apellidoD;
    private LocalDate fecha;

    public String getNombreD() {
        return nombreD;
    }

    public void setNombreD(String nombreD) {
        this.nombreD = nombreD;
    }

    public String getCedulaD() {
        return cedulaD;
    }

    public void setCedulaD(String cedulaD) {
        this.cedulaD = cedulaD;
    }

    public String getApellidoD() {
        return apellidoD;
    }

    public void setApellidoD(String apellidoD) {
        this.apellidoD = apellidoD;
    }
    private LocalTime hora;
    private ColorCita color;
    private String colorN;

    /**
     * Contructor del objeto Cita
     *
     * @param paciente Paciente de la Cita
     * @param doctor Doctor de la Cita
     * @param fecha Fecha de la Cita
     * @param hora Hora de la Cita
     * @param color Color de la Cita
     */
    public Cita(String id,Paciente paciente, Doctor doctor, LocalDate fecha, LocalTime hora, ColorCita color) {
        idCita = id;
        this.doctor = doctor;
        this.paciente = paciente;
        this.fecha = fecha;
        this.hora = hora;
        this.color = color;
        nombreP = paciente.getNombre();
        cedulaP = paciente.getCedula();
        apellidoP = paciente.getApellido();
        colorN = color.name();
        nombreD = doctor.getNombre();
        cedulaD = doctor.getCedula();
        apellidoD = doctor.getApellido();
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public LocalTime getHora() {
        return hora;
    }

    public void setHora(LocalTime hora) {
        this.hora = hora;
    }

    public ColorCita getColor() {
        return color;
    }

    public void setColor(ColorCita color) {
        this.color = color;
    }

    public String getNombreP() {
        return nombreP;
    }

    public void setNombreP(String nombreP) {
        this.nombreP = nombreP;
    }

    public String getCedulaP() {
        return cedulaP;
    }

    public void setCedulaP(String cedulaP) {
        this.cedulaP = cedulaP;
    }

    public String getApellidoP() {
        return apellidoP;
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP = apellidoP;
    }

    public String getColorN() {
        return colorN;
    }

    public void setColorN(String colorN) {
        this.colorN = colorN;
    }

    @Override
    public String toString() {
        return "Cita{" + "nombreP=" + nombreP + ", cedulaP=" + cedulaP + ", apellidoP=" + apellidoP + ", colorN=" + colorN + '}';
    }

    public String getIdCita() {
        return idCita;
    }

    public void setIdCita(String idCita) {
        this.idCita = idCita;
    }
    
    
}
