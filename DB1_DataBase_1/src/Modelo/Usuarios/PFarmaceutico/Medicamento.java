/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Usuarios.PFarmaceutico;

/**
 *
 * @author Domenica Barreiro
 */
public class Medicamento {
    
    private String nombre;
    private String marca;
    private String gramaje;
    
    
    public Medicamento (String nombre, String marca, String gramaje){
        this.nombre = nombre;
        this.marca = marca;
        this.gramaje = gramaje;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
   }

    public void setMarca(String marca) {
       this.marca = marca;
    }

    public String getGramaje() {
        return gramaje;
    }

    public void setGramaje(String gramaje) {
        this.gramaje = gramaje;
    }

    @Override
    public String toString() {
        return "Nombre: " + nombre + ", Marca: " + marca + ", Gramaje: " + gramaje;
    }
    
}
