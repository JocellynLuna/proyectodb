/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Usuarios.PDoctor;

import Modelo.Otros.Patologia;
import Modelo.Usuarios.POperador.Paciente;

/**
 *
 * @author Domenica Barreiro
 */
public class Historial_Medico {
    
    private Paciente paciente;
    private String cedulaP;

    public String getCedulaP() {
        return cedulaP;
    }

    public void setCedulaP(String cedulaP) {
        this.cedulaP = cedulaP;
    }

    public String getNombreP() {
        return nombreP;
    }

    public void setNombreP(String nombreP) {
        this.nombreP = nombreP;
    }

    public String getApellidoP() {
        return apellidoP;
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP = apellidoP;
    }

    public String getNomPat() {
        return nomPat;
    }

    public void setNomPat(String nomPat) {
        this.nomPat = nomPat;
    }

    public String getDesPat() {
        return desPat;
    }

    public void setDesPat(String desPat) {
        this.desPat = desPat;
    }
    private String nombreP;
    private String apellidoP;
    private Patologia patologia;
    private String nomPat;
    private String desPat;
    private String observaciones;
    
    public Historial_Medico(Paciente paciente, Patologia patologia, String obs) {
        this.paciente = paciente;
        this.patologia = patologia;
        this.observaciones = obs;
        this.nombreP = paciente.getNombre();
        this.cedulaP = paciente.getCedula();
        this.apellidoP = paciente.getApellido();
        this.nomPat = patologia.getNombre();
        this.desPat = patologia.getDescripcion();
        
    }

    
    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Patologia getPatologia() {
        return patologia;
    }

    public void setPatología(Patologia patología) {
        this.patologia = patología;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Override
    public String toString() {
        return "Historial_Medico{" + "cedulaP=" + cedulaP + ", nombreP=" + nombreP + ", apellidoP=" + apellidoP + ", nomPat=" + nomPat + ", desPat=" + desPat + ", observaciones=" + observaciones + '}';
    }
    
    
}