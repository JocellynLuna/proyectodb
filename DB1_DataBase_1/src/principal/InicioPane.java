/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import funcional.Util;
import java.io.FileNotFoundException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author joanp y Domenica Barreiro
 */
public class InicioPane {
    
    //private Stage stage;
    private VBox root;
    
    public InicioPane() throws FileNotFoundException{
        
        root = new VBox();
        ImageView logo = Util.imagen("src/recursos/logo.PNG",500,250);
        ImageView doctor = Util.imagen("src/recursos/doctor.jpg",250,250);
        ImageView operador = Util.imagen("src/recursos/operador.png",250,250);
        ImageView farmaceutico = Util.imagen("src/recursos/farmaceutico.jpg",250,250);
        ImageView administrador = Util.imagen("src/recursos/administrador.png",250,250);
        
        Button l = new Button("Ingreso como Doctor",doctor);
        l.setContentDisplay(ContentDisplay.TOP); 
        l.setFont(Font.font("Verdana",20));
        
        Button o = new Button("Ingreso como Operador",operador);
        o.setContentDisplay(ContentDisplay.TOP); 
        o.setFont(Font.font("Verdana",20));
        
        Button f = new Button("Ingreso como Farmacéutico",farmaceutico);
        f.setContentDisplay(ContentDisplay.TOP); 
        f.setFont(Font.font("Verdana",20));
        
        Button a = new Button("Ingreso como Administrador",administrador);
        a.setContentDisplay(ContentDisplay.TOP); 
        a.setFont(Font.font("Verdana",20));
        
        
        /*l.setOnAction(e -> {
            try {
                LoginPane lp = new LoginPane(stage);
            } catch (FileNotFoundException ex) { }
        });
        
        o.setOnAction(e -> {
            try {
                LoginPane lp = new LoginPane(stage);
            } catch (FileNotFoundException ex) { }
        });
        
        f.setOnAction(e -> {
            try {
                LoginPane lp = new LoginPane(stage);
            } catch (FileNotFoundException ex) { } 
        });
        
        a.setOnAction(e -> {
            try {
                LoginPane lp = new LoginPane(stage);
            } catch (FileNotFoundException ex) { } 
        });*/
        
        
        HBox hb = new HBox();
        hb.getChildren().addAll(l,o,f,a); hb.setAlignment(Pos.CENTER);
        hb.setSpacing(100);
        
        Text text = Util.styleText("Bienvenido al Sistema de Gestión", "Verdana", 50,true);
        //text.setStyle("-fx-font-weight: bold");
        
        root.getChildren().addAll(logo,text,hb);   
        root.setAlignment(Pos.CENTER); 
        root.setStyle("-fx-background-color: #ffffff");
        root.setSpacing(75);
        
    }
    
    public VBox getRoot() {
        return root;
    }
}
