/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseño.Operador;

import BaseDeDatosConexion.ConexionBD;
import Modelo.Usuarios.POperador.Paciente;
import funcional.Util;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author joanp
 */
public final class NuevoPaciente {
    private final BorderPane root;
    private Stage stage;
    private Paciente paciente; 
    private TextField cedula;
    private TextField nombre;
    private TextField apellido;
    private TextArea direccion;
    private TextField telefono;
    
    public NuevoPaciente(Stage stage) throws FileNotFoundException{
        this.stage = stage;
        root = new BorderPane();
        paciente = ModificarPaciente.pacienteModific;
        
        root.setTop(Util.operadorTop());
        root.setCenter(Util.operadorCenter("Nuevo Paciente", "Ingrese los datos del paciente a ser atendido", 250));
        root.setBottom(pacienteBottom());
        llenarText();
        
        root.setStyle("-fx-background-image: url('"+getClass().getResource("/recursos/fondo_paciente.jpg").toExternalForm()+"');"
                                        + "-fx-background-repeat: stretch;"  
                                       + "-fx-background-size: "+stage.getWidth()+" "+stage.getHeight()+";"                
                                        + "-fx-background-position: center;");
    }
    
    
    public Pane pacienteBottom() throws FileNotFoundException{
        AnchorPane ap = new AnchorPane();
        
        GridPane gridPane = datos();  
        
        Button guardar = Util.botonImagen("  Guardar ","src/recursos/guardar.png", 50, 50);
        Button regresar = Util.botonImagen("  Regresar","src/recursos/regresar.png", 50, 50);
        regresar.setOnAction(new ManejadorRegresar());
        
        VBox botones = new VBox(new HBox(),guardar,regresar);
        botones.setSpacing(50);
        HBox result = new HBox(gridPane,botones);
        result.setAlignment(Pos.CENTER);
        result.setSpacing(350);
        result.setPrefHeight(350);
        
        ap.getChildren().add(result);
        AnchorPane.setLeftAnchor(result, 250.0);
        AnchorPane.setTopAnchor(result, 0.0);
        AnchorPane.setBottomAnchor(result, 50.0);

        return ap;
    }
    
    private void llenarText(){
        
        if(paciente!=null){
            cedula.setText(paciente.getCedula());
            cedula.setDisable(true);
            nombre.setText(paciente.getNombre());
            nombre.setDisable(true);
            apellido.setText(paciente.getApellido());
            apellido.setDisable(true);
            
            direccion.setText(paciente.getDireccion());
            
            telefono.setText(paciente.getTelefono());
        }
    }
    
    public void comparar(){        
        if(!direccion.getText().equals(paciente.getDireccion())){
            paciente.setDireccion(direccion.getText());
        }
        
        if(!telefono.getText().equals(paciente.getTelefono())){
            paciente.setTelefono(telefono.getText());
        }
    }
    
    public void setDireccion(TextArea direccion) throws SQLException {
        String cedu = cedula.getText();
        ConexionBD.hacerQuery("UPDATE Paciente SET Direccion = direccion WHERE Cedula = cedu");
        this.direccion = direccion;
    }

    public void setTelefono(TextField telefono) throws SQLException {
        String tel = telefono.getText();
        ConexionBD.hacerQuery("UPDATE Paciente SET Telefono = telefono WHERE Cedula = cedu");
        this.telefono = telefono;
    }
    
    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public void setCedula(TextField cedula) {
        this.cedula = cedula;
    }

    public void setNombre(TextField nombre) {
        this.nombre = nombre;
    }

    public void setApellido(TextField apellido) {
        this.apellido = apellido;
    }

    
    
    public GridPane datos(){
        GridPane gridPane = new GridPane(); 
        
        cedula = Util.sizeTextField(525,40);
        nombre = Util.sizeTextField(525,40);
        apellido = Util.sizeTextField(525,40);
        direccion = Util.sizeTextArea(525,80);
        telefono = Util.sizeTextField(525,40);
        
        gridPane.setVgap(20); 
        gridPane.setHgap(100);
        gridPane.add(Util.styleText("Cédula","Verdana",30,false), 0, 0); 
        gridPane.add(cedula, 1, 0); 
        gridPane.add(Util.styleText("Nombre","Verdana",30,false), 0, 1);       
        gridPane.add(nombre, 1, 1); 
        gridPane.add(Util.styleText("Apellido","Verdana",30,false), 0, 2); 
        gridPane.add(apellido, 1, 2);
        gridPane.add(Util.styleText("Dirección","Verdana",30,false), 0, 3); 
        gridPane.add(direccion, 1, 3);
        gridPane.add(Util.styleText("Teléfono","Verdana",30,false), 0, 4); 
        gridPane.add(telefono, 1, 4);
        
        return gridPane;
    }

    public BorderPane getRoot() {
        return root;
    }

    public Stage getOrigen() {
        return stage;
    }
     
    private class ManejadorRegresar implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent ae) {
            try {
                SesionOperador so = new SesionOperador(stage);
                stage.setTitle("Sistema de Gestión - Centro dermatológico Dr. Uraga");
                stage.setScene(so.getScene());
                stage.centerOnScreen (); 
                stage.setMaximized(true);
                stage.show();
            } catch (FileNotFoundException ex) {
            }
        } 
    }

    public Stage getStage() {
        return stage;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public TextField getCedula() {
        return cedula;
    }

    public TextField getNombre() {
        return nombre;
    }

    public TextField getApellido() {
        return apellido;
    }

    public TextArea getDireccion() {
        return direccion;
    }

    public TextField getTelefono() {
        return telefono;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    
    
    
}
